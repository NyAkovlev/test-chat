// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// ManagerGenerator
// **************************************************************************

import 'package:app_di/modules/file_module.dart';
import 'package:app_di/modules/network_module.dart';
import 'package:app_di/modules/platform_module.dart';
import 'package:app_di/modules/security_module.dart';
import 'package:app_di/modules/storage_module.dart';
import 'package:app_di/modules/mapper_module.dart';
import 'package:app_di/modules/worker_module.dart';
import 'package:app_di/modules/socket_module.dart';
import 'package:app_di/app_di.dart';
import 'package:domain/src/api/file/cached_file_api.dart';
import 'package:domain/src/api/file/directory_path_api.dart';
import 'package:domain/src/api/file/file_picker_api.dart';
import 'package:domain_impl/src/api/network/client/graph_ql_client.dart';
import 'package:domain_impl/src/api/network/client/rest_client.dart';
import 'package:domain/src/api/network/auth_api.dart';
import 'package:domain/src/api/network/device_api.dart';
import 'package:domain/src/api/network/user_api.dart';
import 'package:domain/src/api/network/client/token_invalidate_listener.dart';
import 'package:domain/src/api/network/file_network_api.dart';
import 'package:domain/src/api/network/chat_api.dart';
import 'package:domain/src/api/network/message_api.dart';
import 'package:domain/src/api/network/image_url_api.dart';
import 'package:domain/src/api/platform/device_info_api.dart';
import 'package:domain/src/api/security/security_generator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:domain/src/api/storage/auth_pref.dart';
import 'package:domain/src/api/storage/user_pref.dart';
import 'package:domain/src/api/storage/device_pref.dart';
import 'package:domain/src/mapper/model_mapper.dart';
import 'package:domain/src/model/data/user_model.dart';
import 'package:domain/src/model/data/device_model.dart';
import 'package:domain/src/model/response/auth_response.dart';
import 'package:domain/src/model/data/chat_user/chat_user_role.dart';
import 'package:domain/src/model/data/chat_model.dart';
import 'package:domain/src/model/data/chat_user/chat_user_model.dart';
import 'package:domain/src/model/data/message/message_model.dart';
import 'package:domain/src/model/data/message/message_content_model.dart';
import 'package:domain/src/model/data/message/message_content_object_model.dart';
import 'package:intl/src/intl/date_format.dart';
import 'package:domain_worker/src/user/my_user_worker.dart';
import 'package:domain_worker/src/user/user_worker.dart';
import 'package:domain_worker/src/auth/auth_worker.dart';
import 'package:domain_worker/src/file/file_worker.dart';
import 'package:domain_worker/src/chat/chat_worker.dart';
import 'package:domain_worker/src/chat/chat_user_worker.dart';
import 'package:domain_worker/src/messages/message_worker.dart';
import 'package:domain_worker/src/user/user_edition_worker.dart';
import 'package:domain/src/api/socket/socket_controller.dart';
import 'package:app_bloc/src/bloc/user/user/bloc.dart';
import 'package:app_bloc/src/bloc/auth/auth/bloc.dart';
import 'package:app_bloc/src/bloc/error/bloc.dart';
import 'package:app_bloc/src/bloc/auth/token_invalidation/bloc.dart';
import 'package:app_bloc/src/bloc/file/uploader/bloc.dart';
import 'package:app_bloc/src/bloc/user/search/bloc.dart';
import 'dart:core';
import 'package:app_bloc/src/bloc/chat/pagination/bloc.dart';
import 'package:app_bloc/src/bloc/chat/creation/bloc.dart';
import 'package:app_bloc/src/bloc/message/listener/bloc.dart';
import 'package:domain/src/api/socket/message_socket_controller.dart';
import 'package:app_bloc/src/bloc/message/sending/bloc.dart';
import 'package:app_bloc/src/bloc/message/pagination/bloc.dart';
import 'package:app_bloc/src/bloc/user/change_avatar/bloc.dart';

class AppDiImpl extends AppDi {
  final FileModule _fileModule;
  final NetworkModule _networkModule;
  final PlatformModule _platformModule;
  final SecurityModule _securityModule;
  final StorageModule _storageModule;
  final MapperModule _mapperModule;
  final WorkerModule _workerModule;
  final SocketModule _socketModule;

  AppDiImpl({
    FileModule? fileModule,
    NetworkModule? networkModule,
    PlatformModule? platformModule,
    SecurityModule? securityModule,
    StorageModule? storageModule,
    MapperModule? mapperModule,
    WorkerModule? workerModule,
    SocketModule? socketModule,
  })  : this._fileModule = fileModule ?? FileModule(),
        this._networkModule = networkModule ?? NetworkModule(),
        this._platformModule = platformModule ?? PlatformModule(),
        this._securityModule = securityModule ?? SecurityModule(),
        this._storageModule = storageModule ?? StorageModule(),
        this._mapperModule = mapperModule ?? MapperModule(),
        this._workerModule = workerModule ?? WorkerModule(),
        this._socketModule = socketModule ?? SocketModule();

  late CachedFileApi _cachedFileApi;
  DirectoryPathApi? _directoryPathApi;
  FilePickerApi? _filePickerApi;
  MyGraphQlClient? _myGraphQlClient;
  RestClient? _restClient;
  AuthApi? _authApi;
  DeviceApi? _deviceApi;
  UserApi? _userApi;
  TokenInvalidateListener? _tokenInvalidateListener;
  FileNetworkApi? _fileNetworkApi;
  ChatApi? _chatApi;
  MessageApi? _messageApi;
  ImageUrlApi? _imageUrlApi;
  DeviceInfoApi? _deviceInfoApi;
  SecurityGenerator? _securityGenerator;
  late SharedPreferences _sharedPreferences;
  AuthPref? _authPref;
  UserPref? _userPref;
  DevicePref? _devicePref;
  ModelMapper<UserModel>? _modelMapperUserModel;
  ModelMapper<DeviceModel>? _modelMapperDeviceModel;
  ModelMapper<AuthResponse>? _modelMapperAuthResponse;
  ModelMapper<ChatUserRole>? _modelMapperChatUserRole;
  ModelMapper<ChatModel>? _modelMapperChatModel;
  ModelMapper<ChatUserModel>? _modelMapperChatUserModel;
  ModelMapper<MessageModel>? _modelMapperMessageModel;
  ModelMapper<MessageContentModel>? _modelMapperMessageContentModel;
  ModelMapper<MessageContentItemModel>? _modelMapperMessageContentItemModel;
  DateFormat? _dateFormat;
  MyUserWorker? _myUserWorker;
  UserWorker? _userWorker;
  AuthWorker? _authWorker;
  FileWorker? _fileWorker;
  ChatWorker? _chatWorker;
  ChatUserWorker? _chatUserWorker;
  MessageWorker? _messageWorker;
  UserEditionWorker? _userEditionWorker;
  SocketController? _socketController;

  Future<AppDi> init() async {
    _cachedFileApi = await _fileModule.cachedFileApi(_getDirectoryPathApi);
    _sharedPreferences = await _storageModule.sharedPreferences();
    return this;
  }

  CachedFileApi get _getCachedFileApi => _cachedFileApi;
  DirectoryPathApi get _getDirectoryPathApi =>
      _directoryPathApi ??= _fileModule.directoryPathApi();
  FilePickerApi get _getFilePickerApi =>
      _filePickerApi ??= _fileModule.filePickerApi();
  MyGraphQlClient get _getMyGraphQlClient =>
      _myGraphQlClient ??= _networkModule.myGraphQlClient(
          _getAuthPref,
          _getDevicePref,
          _getModelMapperAuthResponse,
          _getTokenInvalidateListener);
  RestClient get _getRestClient => _restClient ??= _networkModule.restClient();
  AuthApi get _getAuthApi => _authApi ??=
      _networkModule.authApi(_getMyGraphQlClient, _getModelMapperAuthResponse);
  DeviceApi get _getDeviceApi => _deviceApi ??=
      _networkModule.deviceApi(_getMyGraphQlClient, _getModelMapperDeviceModel);
  UserApi get _getUserApi => _userApi ??=
      _networkModule.userApi(_getMyGraphQlClient, _getModelMapperUserModel);
  TokenInvalidateListener get _getTokenInvalidateListener =>
      _tokenInvalidateListener ??= _networkModule.tokenInvalidateListener();
  FileNetworkApi get _getFileNetworkApi => _fileNetworkApi ??= _networkModule
      .fileApi(_getRestClient, _getSecurityGenerator, _getAuthPref);
  ChatApi get _getChatApi => _chatApi ??= _networkModule.chatApi(
      _getMyGraphQlClient,
      _getModelMapperChatUserRole,
      _getModelMapperChatModel,
      _getModelMapperChatUserModel);
  MessageApi get _getMessageApi => _messageApi ??= _networkModule.messageApi(
      _getMyGraphQlClient,
      _getModelMapperMessageModel,
      _getModelMapperMessageContentModel,
      _getDateFormat);
  ImageUrlApi get _getImageUrlApi =>
      _imageUrlApi ??= _networkModule.imageUrlApi();
  DeviceInfoApi get _getDeviceInfoApi =>
      _deviceInfoApi ??= _platformModule.deviceInfoApi();
  SecurityGenerator get _getSecurityGenerator =>
      _securityGenerator ??= _securityModule.securityGenerator();
  SharedPreferences get _getSharedPreferences => _sharedPreferences;
  AuthPref get _getAuthPref =>
      _authPref ??= _storageModule.authPref(_getSharedPreferences);
  UserPref get _getUserPref => _userPref ??=
      _storageModule.userPref(_getSharedPreferences, _getModelMapperUserModel);
  DevicePref get _getDevicePref =>
      _devicePref ??= _storageModule.devicePref(_getSharedPreferences);
  ModelMapper<UserModel> get _getModelMapperUserModel =>
      _modelMapperUserModel ??= _mapperModule.userModelMapper();
  ModelMapper<DeviceModel> get _getModelMapperDeviceModel =>
      _modelMapperDeviceModel ??= _mapperModule.deviceModelMapper();
  ModelMapper<AuthResponse> get _getModelMapperAuthResponse =>
      _modelMapperAuthResponse ??= _mapperModule.authResponseMapper();
  ModelMapper<ChatUserRole> get _getModelMapperChatUserRole =>
      _modelMapperChatUserRole ??= _mapperModule.chatUserRoleMapper();
  ModelMapper<ChatModel> get _getModelMapperChatModel =>
      _modelMapperChatModel ??= _mapperModule.chatMapper();
  ModelMapper<ChatUserModel> get _getModelMapperChatUserModel =>
      _modelMapperChatUserModel ??= _mapperModule.chatUserModelMapper();
  ModelMapper<MessageModel> get _getModelMapperMessageModel =>
      _modelMapperMessageModel ??=
          _mapperModule.messageMapper(_getModelMapperMessageContentModel);
  ModelMapper<MessageContentModel> get _getModelMapperMessageContentModel =>
      _modelMapperMessageContentModel ??= _mapperModule
          .messageContentMapper(_getModelMapperMessageContentItemModel);
  ModelMapper<MessageContentItemModel>
      get _getModelMapperMessageContentItemModel =>
          _modelMapperMessageContentItemModel ??=
              _mapperModule.messageContentItemMapper();
  DateFormat get _getDateFormat => _dateFormat ??= _mapperModule.dateFormat();
  MyUserWorker get _getMyUserWorker =>
      _myUserWorker ??= _workerModule.myUserWorker(_getUserApi, _getUserPref);
  UserWorker get _getUserWorker =>
      _userWorker ??= _workerModule.userWorker(_getUserApi);
  AuthWorker get _getAuthWorker => _authWorker ??= _workerModule.authWorker(
      _getDevicePref,
      _getDeviceApi,
      _getDeviceInfoApi,
      _getAuthApi,
      _getAuthPref,
      _getSecurityGenerator);
  FileWorker get _getFileWorker =>
      _fileWorker ??= _workerModule.fileCacheWorker(_getFileNetworkApi,
          _getCachedFileApi, _getFilePickerApi, _getSecurityGenerator);
  ChatWorker get _getChatWorker =>
      _chatWorker ??= _workerModule.chatWorker(_getChatApi);
  ChatUserWorker get _getChatUserWorker =>
      _chatUserWorker ??= _workerModule.chatUserWorker(_getChatApi);
  MessageWorker get _getMessageWorker =>
      _messageWorker ??= _workerModule.messageWorker(_getMessageApi);
  UserEditionWorker get _getUserEditionWorker =>
      _userEditionWorker ??= _workerModule.userEditionWorker(
          _getUserApi, _getUserPref, _getFileWorker);
  SocketController get _getSocketController =>
      _socketController ??= _socketModule.socketController(
          _getAuthPref,
          _getSecurityGenerator,
          _getModelMapperMessageContentModel,
          _getModelMapperMessageModel);

  AuthPref authPref() => _getAuthPref;
  SocketController socketController() => _getSocketController;
  ImageUrlApi imageUrlApi() => _getImageUrlApi;
  UserBloc userBloc() => UserBloc(
        _getUserPref,
        _getMyUserWorker,
      );
  AuthBloc authBloc() => AuthBloc(
        _getAuthWorker,
        _getMyUserWorker,
      );
  ErrorBloc errorBloc() => ErrorBloc();
  InvalidationTokenBloc invalidationTokenBloc() => InvalidationTokenBloc(
        _getTokenInvalidateListener,
        _getUserPref,
        _getAuthPref,
      );
  FileUploaderBloc fileUploadBloc() => FileUploaderBloc(
        _getFileWorker,
      );
  UserPaginationBloc userPaginationBloc(
    String _search,
  ) =>
      UserPaginationBloc(
        _search,
        _getUserWorker,
      );
  ChatPaginationBloc chatPaginationBloc() => ChatPaginationBloc(
        _getChatWorker,
      );
  ChatCreationBloc chatCreationBloc() => ChatCreationBloc(
        _getChatApi,
      );
  MessageListenerBloc messageListenerBloc(
    MessageSocketController _messageSocketController,
  ) =>
      MessageListenerBloc(
        _messageSocketController,
        _getMessageWorker,
      );
  MessageSendingBloc messageSendingBloc(
    ChatModel _chatModel,
  ) =>
      MessageSendingBloc(
        _getMessageWorker,
        _chatModel,
      );
  MessagePaginationBloc messagePaginationBloc(
    ChatModel _chatModel,
  ) =>
      MessagePaginationBloc(
        _getMessageWorker,
        _chatModel,
      );
  UserChangeAvatarBloc userChangeAvatarBloc() => UserChangeAvatarBloc(
        _getUserEditionWorker,
      );
}
