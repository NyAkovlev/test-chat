class DiConfig {
  static const String restUrl = "$_url/api";
  static const String graphQlUrl = "$_url/graphql";
  static const String webSocketUrl = "$_url/ws";
  static const String imageUrl = "$_url/api/files";
  static const String _url = "http://192.168.31.214:8080";
}
