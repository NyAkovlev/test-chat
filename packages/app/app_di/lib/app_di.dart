import 'package:app_bloc/export.dart';
import 'package:app_di/modules/file_module.dart';
import 'package:app_di/modules/network_module.dart';
import 'package:app_di/modules/platform_module.dart';
import 'package:app_di/modules/security_module.dart';
import 'package:app_di/modules/socket_module.dart';
import 'package:app_di/modules/storage_module.dart';
import 'package:dm/dm.dart';
import 'package:domain/export.dart';

import 'app_di.dm.dart';
import 'modules/mapper_module.dart';
import 'modules/worker_module.dart';

@Modules([
  FileModule,
  NetworkModule,
  PlatformModule,
  SecurityModule,
  StorageModule,
  MapperModule,
  WorkerModule,
  SocketModule,
])
abstract class AppDi {
  static late AppDi instance;

  static Future<AppDi> init() async {
    return instance = await AppDiImpl().init();
  }

  @GetInstance()
  AuthPref authPref();

  @GetInstance()
  SocketController socketController();

  @GetInstance()
  ImageUrlApi imageUrlApi();

  UserBloc userBloc();

  AuthBloc authBloc();

  ErrorBloc errorBloc();

  InvalidationTokenBloc invalidationTokenBloc();

  FileUploaderBloc fileUploadBloc();

  UserPaginationBloc userPaginationBloc(String _search);

  ChatPaginationBloc chatPaginationBloc();

  ChatCreationBloc chatCreationBloc();

  MessageListenerBloc messageListenerBloc(
    MessageSocketController _messageSocketController,
  );

  MessageSendingBloc messageSendingBloc(
    ChatModel _chatModel,
  );

  MessagePaginationBloc messagePaginationBloc(ChatModel _chatModel);

  UserChangeAvatarBloc userChangeAvatarBloc();
}
