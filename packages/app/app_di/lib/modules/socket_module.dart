import 'package:app_di/config.dart';
import 'package:dm/dm.dart';
import 'package:domain/export.dart';
import 'package:domain_impl/export.dart';

class SocketModule {
  @Provide()
  SocketController socketController(
    AuthPref authPref,
    SecurityGenerator securityGenerator,
    ModelMapper<MessageContentModel> messageContentMapper,
    ModelMapper<MessageModel> messageMapper,
  ) =>
      SocketFactory.createController(
        DiConfig.webSocketUrl,
        authPref,
        securityGenerator,
        messageContentMapper,
        messageMapper,
      );
}
