import 'package:dm/dm.dart';
import 'package:domain/export.dart';
import 'package:domain_impl/export.dart';

class FileModule {
  @Provide()
  Future<CachedFileApi> cachedFileApi(
          DirectoryPathApi directoryPathApi) async =>
      CachedFileApiImpl(await directoryPathApi.tempDir());

  @Provide()
  DirectoryPathApi directoryPathApi() => DirectoryPathApiImpl();

  @Provide()
  FilePickerApi filePickerApi() => FilePickerApiImpl();
}
