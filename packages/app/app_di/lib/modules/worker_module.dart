import 'package:dm/dm.dart';
import 'package:domain/export.dart';
import 'package:domain_worker/domain_worker.dart';

class WorkerModule {
  @Provide()
  MyUserWorker myUserWorker(
    UserApi userApi,
    UserPref userPref,
  ) =>
      MyUserWorker(
        userApi,
        userPref,
      );
  @Provide()
  UserWorker userWorker(
      UserApi userApi,
      ) =>
      UserWorker(
        userApi,
      );
  @Provide()
  AuthWorker authWorker(
    DevicePref devicePref,
    DeviceApi deviceApi,
    DeviceInfoApi deviceInfoApi,
    AuthApi authApi,
    AuthPref authPref,
    SecurityGenerator securityGenerator,
  ) =>
      AuthWorker(
        devicePref,
        deviceApi,
        deviceInfoApi,
        securityGenerator,
        authApi,
        authPref,
      );

  @Provide()
  FileWorker fileCacheWorker(
          FileNetworkApi fileApi,
          CachedFileApi cachedFileApi,
          FilePickerApi filePickerApi,
          SecurityGenerator securityGenerator) =>
      FileWorker(
        fileApi,
        cachedFileApi,
        filePickerApi,
        securityGenerator,
      );

  @Provide()
  ChatWorker chatWorker(ChatApi chatApi) => ChatWorker(chatApi);

  @Provide()
  ChatUserWorker chatUserWorker(ChatApi chatApi) => ChatUserWorker(chatApi);

  @Provide()
  MessageWorker messageWorker(MessageApi messageApi) =>
      MessageWorker(messageApi);

  @Provide()
  UserEditionWorker userEditionWorker(
    UserApi _userApi,
    UserPref _userPref,
    FileWorker _fileWorker,
  ) =>
      UserEditionWorker(_userApi, _userPref, _fileWorker);
}
