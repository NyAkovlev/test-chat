import 'package:dm/dm.dart';
import 'package:domain/export.dart';
import 'package:domain_impl/export.dart';
import 'package:intl/intl.dart';

class MapperModule {
  @Provide()
  ModelMapper<UserModel> userModelMapper() => UserModelMapper();

  @Provide()
  ModelMapper<DeviceModel> deviceModelMapper() => DeviceModelMapper();

  @Provide()
  ModelMapper<AuthResponse> authResponseMapper() => AuthResponseMapper();

  @Provide()
  ModelMapper<ChatUserRole> chatUserRoleMapper() => ChatUserRoleMapper();

  @Provide()
  ModelMapper<ChatModel> chatMapper() => ChatModelMapper();

  @Provide()
  ModelMapper<ChatUserModel> chatUserModelMapper() => ChatUserMapper();

  @Provide()
  ModelMapper<MessageModel> messageMapper(
          ModelMapper<MessageContentModel> mapper) =>
      MessageMapper(mapper);

  @Provide()
  ModelMapper<MessageContentModel> messageContentMapper(
          ModelMapper<MessageContentItemModel> mapper) =>
      MessageContentMapper(mapper);

  @Provide()
  ModelMapper<MessageContentItemModel> messageContentItemMapper() =>
      MessageContentItemMapper();
  @Provide()
  DateFormat dateFormat() => DateFormat("yyyy-MM-ddTHH:mm:ssZ");
}
