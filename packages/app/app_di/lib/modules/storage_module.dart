import 'package:dm/dm.dart';
import 'package:domain/export.dart';
import 'package:domain_impl/export.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StorageModule {
  @Provide()
  Future<SharedPreferences> sharedPreferences() =>
      SharedPreferences.getInstance();

  @Provide()
  AuthPref authPref(SharedPreferences sharedPreferences) =>
      AuthPrefImpl(sharedPreferences);

  @Provide()
  UserPref userPref(
    SharedPreferences sharedPreferences,
    ModelMapper<UserModel> mapper,
  ) =>
      UserPrefImpl(sharedPreferences, mapper);

  @Provide()
  DevicePref devicePref(SharedPreferences sharedPreferences) =>
      DevicePrefImpl(sharedPreferences);
}
