import 'package:dm/dm.dart';
import 'package:domain/export.dart';
import 'package:domain_impl/export.dart';

class SecurityModule {
  @Provide()
  SecurityGenerator securityGenerator() => SecurityGeneratorImpl();
}
