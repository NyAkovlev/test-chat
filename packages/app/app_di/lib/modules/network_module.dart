import 'package:app_di/config.dart';
import 'package:dm/dm.dart';
import 'package:domain/export.dart';
import 'package:domain_impl/export.dart';
import 'package:intl/intl.dart';

class NetworkModule {
  @Provide()
  MyGraphQlClient myGraphQlClient(
    AuthPref authPref,
    DevicePref devicePref,
    ModelMapper<AuthResponse> mapper,
    TokenInvalidateListener tokenInvalidateListener,
  ) =>
      MyGraphQlClient(
        authPref,
        DiConfig.graphQlUrl,
        devicePref,
        mapper,
        tokenInvalidateListener,
      );

  @Provide()
  RestClient restClient() => RestClient(DiConfig.restUrl);

  @Provide()
  AuthApi authApi(MyGraphQlClient client, ModelMapper<AuthResponse> mapper) =>
      AuthApiImpl(client, mapper);

  @Provide()
  DeviceApi deviceApi(
          MyGraphQlClient client, ModelMapper<DeviceModel> mapper) =>
      DeviceApiImpl(client, mapper);

  @Provide()
  UserApi userApi(MyGraphQlClient client, ModelMapper<UserModel> mapper) =>
      UserApiImpl(client, mapper);

  @Provide()
  TokenInvalidateListener tokenInvalidateListener() =>
      TokenInvalidateListenerImpl();

  @Provide()
  FileNetworkApi fileApi(RestClient restClient,
          SecurityGenerator securityGenerator, AuthPref authPref) =>
      FileNetworkApiImpl(
        restClient,
        securityGenerator,
        authPref,
      );

  @Provide()
  ChatApi chatApi(
    MyGraphQlClient client,
    ModelMapper<ChatUserRole> chatUserRoleMapper,
    ModelMapper<ChatModel> chatMapper,
    ModelMapper<ChatUserModel> chatUserModelMapper,
  ) =>
      ChatApiImpl(
        client,
        chatUserRoleMapper,
        chatMapper,
        chatUserModelMapper,
      );

  @Provide()
  MessageApi messageApi(
    MyGraphQlClient _client,
    ModelMapper<MessageModel> _mapper,
    ModelMapper<MessageContentModel> _contentMapper,
    DateFormat dateFormat,
  ) =>
      MessageApiImpl(
        _client,
        _mapper,
        dateFormat,
        _contentMapper,
      );

  @Provide()
  ImageUrlApi imageUrlApi() => ImageUrlApiImpl(DiConfig.imageUrl);
}
