import 'package:dm/dm.dart';
import 'package:domain/export.dart';
import 'package:domain_impl/export.dart';

class PlatformModule {
  @Provide()
  DeviceInfoApi deviceInfoApi() => DeviceInfoApiImpl();
}
