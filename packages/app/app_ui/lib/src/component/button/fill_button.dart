import 'package:flutter/material.dart';

class FillButton extends StatelessWidget {
  final String text;
  final VoidCallback voidCallback;

  const FillButton({
    required this.text,
    required this.voidCallback,
  });

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      child: Text(text),
      onPressed: voidCallback,
    );
  }
}
