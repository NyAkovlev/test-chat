import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/single_child_widget.dart';

typedef BlocWidgetListener<S> = void Function(BuildContext context, S state);

typedef BlocCondition<S> = bool Function(S? previous, S current);

class BlocListener<B extends BlocBase<S>, S> extends SingleChildStatefulWidget {
  final Widget? child;

  final B bloc;

  final BlocWidgetListener<S>? listener;

  final BlocCondition<S>? listenWhen;

  const BlocListener({
    Key? key,
    this.listener,
    required this.bloc,
    this.listenWhen,
    this.child,
  }) : super(key: key, child: child);

  @override
  _BlocListenerBaseState<B, S> createState() => _BlocListenerBaseState<B, S>();
}

class _BlocListenerBaseState<B extends BlocBase<S>, S>
    extends SingleChildState<BlocListener<B, S>> {
  StreamSubscription<S>? _subscription;
  late B _cubit;
  S? _previousState;

  @override
  void initState() {
    super.initState();
    _cubit = widget.bloc;
    _previousState = _cubit.state;
    _subscribe();
  }

  @override
  void didUpdateWidget(BlocListener<B, S> oldWidget) {
    super.didUpdateWidget(oldWidget);
    final oldCubit = oldWidget.bloc;
    final currentCubit = widget.bloc;
    if (oldCubit != currentCubit) {
      if (_subscription != null) {
        _unsubscribe();
        _cubit = currentCubit;
        _previousState = _cubit.state;
      }
      _subscribe();
    }
  }

  @override
  Widget buildWithChild(BuildContext context, Widget? child) =>
      child ?? SizedBox.shrink();

  @override
  void dispose() {
    _unsubscribe();
    super.dispose();
  }

  void _subscribe() {
    _subscription = _cubit.stream.listen((state) {
      if (widget.listenWhen?.call(_previousState, state) ?? true) {
        widget.listener?.call(context, state);
      }
      _previousState = state;
    });
  }

  void _unsubscribe() {
    _subscription?.cancel();
    _subscription = null;
  }
}
