import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';

import 'bloc_builder.dart';
import 'bloc_listener.dart';
import 'bloc_provider.dart';

class BlocWidget<B extends BlocBase<S>, S> extends StatelessWidget {
  final BlocWidgetListener<S>? listener;
  final BlocWidgetBuilder<S> builder;
  final B bloc;
  final BlocCondition<S>? condition;

  const BlocWidget({
    Key? key,
    this.listener,
    required this.builder,
    required this.bloc,
    this.condition,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget widget = BlocBuilder<B, S>(
      bloc: bloc,
      builder: builder,
      buildWhen: condition,
    );
    if (listener != null) {
      widget = BlocListener<B, S>(
        listener: listener,
        bloc: bloc,
        child: widget,
      );
    }
    return BlocProvider<B>(
      bloc: bloc,
      child: widget,
    );
  }
}
