import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import 'bloc_listener.dart';

typedef BlocWidgetBuilder<S> = Widget Function(BuildContext context, S state);

class BlocBuilder<B extends BlocBase<S>, S> extends StatefulWidget {
  final B bloc;
  final BlocCondition<S>? buildWhen;
  final BlocWidgetBuilder<S> builder;

  const BlocBuilder({
    Key? key,
    required this.builder,
    required this.bloc,
    this.buildWhen,
  }) : super(key: key);

  @override
  State<BlocBuilder<B, S>> createState() => _BlocBuilderBaseState<B, S>();
}

class _BlocBuilderBaseState<B extends BlocBase<S>, S>
    extends State<BlocBuilder<B, S>> {
  late B _cubit;
  late S _state;

  @override
  void initState() {
    super.initState();
    _cubit = widget.bloc;
    _state = _cubit.state;
  }

  @override
  void didUpdateWidget(BlocBuilder<B, S> oldWidget) {
    super.didUpdateWidget(oldWidget);
    final oldCubit = oldWidget.bloc;
    final currentCubit = widget.bloc;
    if (oldCubit != currentCubit) {
      _cubit = currentCubit;
      _state = _cubit.state;
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<B, S>(
      bloc: _cubit,
      listenWhen: widget.buildWhen,
      listener: (context, state) => setState(() => _state = state),
      child: widget.builder(context, _state),
    );
  }
}
