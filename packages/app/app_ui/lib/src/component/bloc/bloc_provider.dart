import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BlocProvider<B extends BlocBase> extends Provider<B> {
  BlocProvider({
    required B bloc,
    required Widget child,
  }) : super.value(child: child, value: bloc);

  static B of<B extends BlocBase>(BuildContext context) {
    try {
      return Provider.of<B>(context, listen: false);
    } catch (e) {
      print(e);
      rethrow;
    }
  }
}
