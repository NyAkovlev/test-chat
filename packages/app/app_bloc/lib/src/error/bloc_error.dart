import 'bloc_error_case.dart';

class BlocError extends Error {
  final String? description;
  final BlocErrorCase errorCase;

  BlocError(
    this.errorCase, [
    this.description,
  ]);
}
