import 'dart:async';
import 'package:async/async.dart';
import 'package:bloc/bloc.dart';
import 'package:util/util.dart';

/// только у последней подписки срабатывает listener при обновлении state
/// это использует [ErrorBloc], что бы ошибка отображалась только в одном месте,
/// хотя этот блок слушает каждый Scaffold
mixin OneSubBloc<E, S> on Bloc<E, S> {
  Set<StreamSubscription> listener = {};

  @override
  StreamSubscription<S> listen(
    void Function(S)? onData, {
    Function? onError,
    void Function()? onDone,
    bool? cancelOnError,
  }) {
    _StreamSubscription<S>? sub;
    sub = _StreamSubscription<S>(super.listen(
      (s) {
        if (listener.lastOrNull == sub) {
          onData?.call(s);
        }
      },
      onError: onError,
      onDone: onDone,
      cancelOnError: cancelOnError,
    ));
    listener.add(sub);
    sub.onCancel = () {
      listener.remove(sub);
    };

    return sub;
  }
}

class _StreamSubscription<T> extends DelegatingStreamSubscription<T> {
  Function? onCancel;

  _StreamSubscription(StreamSubscription<T> sourceSubscription)
      : super(sourceSubscription);

  @override
  Future cancel() {
    if (onCancel != null) {
      onCancel!();
    }
    return super.cancel();
  }
}
