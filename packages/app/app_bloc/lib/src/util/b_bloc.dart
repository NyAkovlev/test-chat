import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:like_sealed/like_sealed.dart';
import 'package:rxdart/rxdart.dart';
import 'package:meta/meta.dart';
import 'package:app_bloc/export.dart';

abstract class BBloc<Event, State> extends Bloc<Event, State>
    implements SealedSwitch<Event, Stream<State>>, Sink<Event> {
  static bool testBloc = false;
  bool _isClosed = false;

  BBloc(State initialState) : super(initialState);

  bool get isClosed => _isClosed;

  @override
  Future<void> close() {
    _isClosed = true;
    return super.close();
  }

  @override
  void add(Event event) {
    super.add(event);
  }

  @override
  @mustCallSuper
  Stream<State> mapEventToState(Event event) {
    return switchCase(event).doOnError((e, s) {
      print(s);
      print(e);
    }).onErrorResume((error) {
      try {
        return mapErrorToState(error).doOnError((e, s) {
          ErrorBloc.error(e);
        });
      } catch (e) {
        ErrorBloc.error(e);
        return Stream.empty();
      }
    });
  }

  Stream<State> mapErrorToState(dynamic error);
}
