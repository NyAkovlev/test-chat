import 'package:equatable/equatable.dart';
import 'package:like_sealed/like_sealed.dart';


class PaginationState<T> extends Equatable {
  final List<T> items;
  final bool hasMore;
  final int page;
  final dynamic error;

  PaginationState(this.items, this.hasMore, this.page, [this.error]);

  @override
  List<Object?> get props => [items.hashCode, hasMore, page];
}
