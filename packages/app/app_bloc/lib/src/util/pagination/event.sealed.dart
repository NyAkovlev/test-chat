// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class PaginationEvents {
  static PaginationEventNextPage nextPage([
    bool clear = false,
  ]) {
    return PaginationEventNextPage(clear);
  }

  static PaginationEventRefresh refresh() {
    return PaginationEventRefresh();
  }
}

abstract class PaginationEventSwitch<O>
    implements SealedSwitch<PaginationEvent, O> {
  O switchCase(PaginationEvent type) => type is PaginationEventNextPage
      ? onNextPage(type)
      : type is PaginationEventRefresh
          ? onRefresh(type)
          : onDefault(type);
  O onNextPage(PaginationEventNextPage nextPage);
  O onRefresh(PaginationEventRefresh refresh);
  O onDefault(PaginationEvent paginationEvent) => throw UnimplementedError();
}
