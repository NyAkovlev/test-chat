import 'package:equatable/equatable.dart';
import 'package:like_sealed/like_sealed.dart';
import 'package:util/util.dart';

part 'event.sealed.dart';

@likeSealed
abstract class PaginationEvent {}

class PaginationEventNextPage extends PaginationEvent {
  final bool clear;

  PaginationEventNextPage([this.clear = false]);
}

class PaginationEventRefresh extends PaginationEvent {}
