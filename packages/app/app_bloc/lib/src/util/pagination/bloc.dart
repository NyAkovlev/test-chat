import 'package:app_bloc/export.dart';
import 'package:domain/export.dart';
import 'package:meta/meta.dart';

import 'export.dart';

abstract class PaginationBloc<T>
    extends BBloc<PaginationEvent, PaginationState<T>>
    with PaginationEventSwitch<Stream<PaginationState<T>>> {
  @protected
  int page = 0;
  var _items = <T>[];
  final int pageSize;

  PaginationBloc({required this.pageSize}) : super(PaginationState([], true, 0));

  @override
  Stream<PaginationState<T>> mapErrorToState(error) async* {
    yield PaginationState(_items, false, page, error);
  }

  @override
  Stream<PaginationState<T>> onRefresh(PaginationEventRefresh refresh) async* {
    yield* onNextPage(PaginationEventNextPage(true));
  }

  @override
  Stream<PaginationState<T>> onNextPage(
    PaginationEventNextPage event,
  ) async* {
    final currentPage = event.clear ? 0 : page;
    final list = await findItems(pageSize, currentPage);
    if (event.clear) {
      page = 0;
      _items = [];
    }
    page++;
    _items = _items.toList();
    _items.addAll(list.items);
    yield PaginationState(
      _items,
      list.items.length == pageSize,
      currentPage,
    );
  }

  Future<ListResponse<T>> findItems(int pageSize, int page);
}
