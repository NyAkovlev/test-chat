import 'package:domain/export.dart';
import 'package:like_sealed/like_sealed.dart';

part 'event.sealed.dart';

@likeSealed
abstract class MessageListenerEvent {}

class MessageListenerEventAddMessage extends MessageListenerEvent {
  final MessageModel message;

  MessageListenerEventAddMessage(this.message);
}
