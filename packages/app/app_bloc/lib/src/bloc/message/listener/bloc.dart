import 'dart:async';

import 'package:app_bloc/export.dart';
import 'package:domain/export.dart';
import 'package:domain_worker/domain_worker.dart';

import 'export.dart';

class MessageListenerBloc
    extends BBloc<MessageListenerEvent, MessageListenerState>
    with MessageListenerEventSwitch<Stream<MessageListenerState>> {
  final MessageSocketController _messageSocketController;
  final MessageWorker _worker;
  late StreamSubscription _sub;
  final List<MessageModel> _messages = [];
  var messageBefore = DateTime.now();

  MessageListenerBloc(
    this._messageSocketController,
    this._worker,
  ) : super(MessageListenerState([])) {
    _sub = _worker.messageStream(
      controller: _messageSocketController,
      onAdd: (v) {
        add(MessageListenerEventAddMessage(v));
      },
    );
  }

  @override
  Future<void> close() {
    _sub.cancel();
    return super.close();
  }

  @override
  Stream<MessageListenerState> mapErrorToState(error) async* {
    yield MessageListenerState(_messages, error);
  }

  @override
  Stream<MessageListenerState> onAddMessage(
      MessageListenerEventAddMessage addMessage) async* {
    _messages.add(addMessage.message);
    yield MessageListenerState(_messages);
  }
}
