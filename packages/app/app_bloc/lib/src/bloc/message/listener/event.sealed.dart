// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class MessageListenerEvents {
  static MessageListenerEventAddMessage addMessage(
    MessageModel message,
  ) {
    return MessageListenerEventAddMessage(message);
  }
}

abstract class MessageListenerEventSwitch<O>
    implements SealedSwitch<MessageListenerEvent, O> {
  O switchCase(MessageListenerEvent type) =>
      type is MessageListenerEventAddMessage
          ? onAddMessage(type)
          : onDefault(type);
  O onAddMessage(MessageListenerEventAddMessage addMessage);
  O onDefault(MessageListenerEvent messageListenerEvent) =>
      throw UnimplementedError();
}
