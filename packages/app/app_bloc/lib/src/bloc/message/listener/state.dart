import 'package:domain/export.dart';
import 'package:like_sealed/like_sealed.dart';
import 'package:util/util.dart';


class MessageListenerState with Unique {
  final List<MessageModel> message;
  final dynamic error;

  MessageListenerState(this.message, [this.error]);
}
