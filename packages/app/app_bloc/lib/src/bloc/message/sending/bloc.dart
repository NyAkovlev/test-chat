import 'package:app_bloc/export.dart';
import 'package:domain/export.dart';
import 'package:domain_worker/domain_worker.dart';

import 'export.dart';

  class MessageSendingBloc extends BBloc<MessageSendingEvent, MessageSendingState>
    with MessageSendingEventSwitch<Stream<MessageSendingState>> {
  final MessageWorker _worker;
  final ChatModel _chatModel;

  MessageSendingBloc(
    this._worker,
    this._chatModel,
  ) : super(MessageSendingStateInit());

  @override
  Stream<MessageSendingState> onSend(MessageSendingEventSend send) async* {
    yield MessageSendingStateProgress();
    await _worker.sendMessage(
      message: send.content,
      chatModel: _chatModel,
    );
    yield MessageSendingStateSuccess();
  }

  @override
  Stream<MessageSendingState> mapErrorToState(error) async* {
    yield MessageSendingStateError(error);
  }
}
