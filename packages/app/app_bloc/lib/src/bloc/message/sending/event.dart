import 'package:domain/export.dart';
import 'package:like_sealed/like_sealed.dart';

part 'event.sealed.dart';

@likeSealed
abstract class MessageSendingEvent {}

class MessageSendingEventSend extends MessageSendingEvent {
  final MessageContentModel content;

  MessageSendingEventSend(this.content);
}
