// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class MessageSendingEvents {
  static MessageSendingEventSend send(
    MessageContentModel content,
  ) {
    return MessageSendingEventSend(content);
  }
}

abstract class MessageSendingEventSwitch<O>
    implements SealedSwitch<MessageSendingEvent, O> {
  O switchCase(MessageSendingEvent type) =>
      type is MessageSendingEventSend ? onSend(type) : onDefault(type);
  O onSend(MessageSendingEventSend send);
  O onDefault(MessageSendingEvent messageSendingEvent) =>
      throw UnimplementedError();
}
