import 'package:equatable/equatable.dart';
import 'package:like_sealed/like_sealed.dart';
import 'package:util/util.dart';

part 'state.sealed.dart';

@likeSealed
abstract class MessageSendingState extends Equatable {
  @override
  List<Object> get props => [];
}

class MessageSendingStateInit extends MessageSendingState {}

class MessageSendingStateProgress extends MessageSendingState {}

class MessageSendingStateSuccess extends MessageSendingState {}

class MessageSendingStateError extends MessageSendingState with Unique {
  final dynamic error;

  MessageSendingStateError(this.error);
}
