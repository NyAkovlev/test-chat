// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class MessageSendingStates {
  static MessageSendingStateInit init() {
    return MessageSendingStateInit();
  }

  static MessageSendingStateProgress progress() {
    return MessageSendingStateProgress();
  }

  static MessageSendingStateSuccess success() {
    return MessageSendingStateSuccess();
  }

  static MessageSendingStateError error(
    dynamic error,
  ) {
    return MessageSendingStateError(error);
  }
}

abstract class MessageSendingStateSwitch<O>
    implements SealedSwitch<MessageSendingState, O> {
  O switchCase(MessageSendingState type) => type is MessageSendingStateInit
      ? onInit(type)
      : type is MessageSendingStateProgress
          ? onProgress(type)
          : type is MessageSendingStateSuccess
              ? onSuccess(type)
              : type is MessageSendingStateError
                  ? onError(type)
                  : onDefault(type);
  O onInit(MessageSendingStateInit init);
  O onProgress(MessageSendingStateProgress progress);
  O onSuccess(MessageSendingStateSuccess success);
  O onError(MessageSendingStateError error);
  O onDefault(MessageSendingState messageSendingState) =>
      throw UnimplementedError();
}
