import 'package:app_bloc/export.dart';
import 'package:domain/export.dart';
import 'package:domain_worker/domain_worker.dart';

class MessagePaginationBloc extends PaginationBloc<MessageModel> {
  final MessageWorker _messageWorker;
  final ChatModel _chatModel;
  DateTime _searchBefore = DateTime.now();

  MessagePaginationBloc(
    this._messageWorker,
    this._chatModel,
  ) : super(pageSize: 50);

  @override
  Future<ListResponse<MessageModel>> findItems(int pageSize, int page) async {
    if (page == 0) {
      _searchBefore = DateTime.now();
    }

    final result = await _messageWorker.messagesOfChatBefore(
      _chatModel.id,
      _searchBefore,
      pageSize,
    );

    if (result.items.isNotEmpty) {
      _searchBefore = result.items.last.created;
    }
    return result;
  }
}
