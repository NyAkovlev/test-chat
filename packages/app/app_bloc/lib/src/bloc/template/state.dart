import 'package:equatable/equatable.dart';
import 'package:like_sealed/like_sealed.dart';

part 'state.sealed.dart';
@likeSealed

abstract class TemplateState extends Equatable {
  @override
  List<Object> get props => [];
}

class TemplateStateInit extends TemplateState {}
