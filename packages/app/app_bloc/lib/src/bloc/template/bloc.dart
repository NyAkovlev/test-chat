import 'export.dart';
import 'package:app_bloc/export.dart';

class TemplateBloc extends BBloc<TemplateEvent, TemplateState>
    with TemplateEventSwitch<Stream<TemplateState>> {
  TemplateBloc() : super(TemplateStateInit());

  @override
  Stream<TemplateState> mapErrorToState(error) {
    throw UnimplementedError();
  }
}
