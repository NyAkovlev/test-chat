// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class TemplateStates {
  static TemplateStateInit init() {
    return TemplateStateInit();
  }
}

abstract class TemplateStateSwitch<O>
    implements SealedSwitch<TemplateState, O> {
  O switchCase(TemplateState type) =>
      type is TemplateStateInit ? onInit(type) : onDefault(type);
  O onInit(TemplateStateInit init);
  O onDefault(TemplateState templateState) => throw UnimplementedError();
}
