// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class TemplateEvents {}

abstract class TemplateEventSwitch<O>
    implements SealedSwitch<TemplateEvent, O> {
  O switchCase(TemplateEvent type) => onDefault(type);
  O onDefault(TemplateEvent templateEvent) => throw UnimplementedError();
}
