// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class FileUploaderStates {
  static FileUploaderStateInit init() {
    return FileUploaderStateInit();
  }

  static FileUploaderStateProgress progress() {
    return FileUploaderStateProgress();
  }

  static FileUploaderStateSuccess success(
    LocalFileModel localFile,
  ) {
    return FileUploaderStateSuccess(localFile);
  }

  static FileUploaderStateError error(
    dynamic error,
  ) {
    return FileUploaderStateError(error);
  }
}

abstract class FileUploaderStateSwitch<O>
    implements SealedSwitch<FileUploaderState, O> {
  O switchCase(FileUploaderState type) => type is FileUploaderStateInit
      ? onInit(type)
      : type is FileUploaderStateProgress
          ? onProgress(type)
          : type is FileUploaderStateSuccess
              ? onSuccess(type)
              : type is FileUploaderStateError
                  ? onError(type)
                  : onDefault(type);
  O onInit(FileUploaderStateInit init);
  O onProgress(FileUploaderStateProgress progress);
  O onSuccess(FileUploaderStateSuccess success);
  O onError(FileUploaderStateError error);
  O onDefault(FileUploaderState fileUploaderState) =>
      throw UnimplementedError();
}
