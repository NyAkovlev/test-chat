// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class FileUploaderEvents {
  static FileUploaderEventUpload upload([
    Future<dynamic> Function(LocalFileModel)? onUpload,
  ]) {
    return FileUploaderEventUpload(onUpload);
  }
}

abstract class FileUploaderEventSwitch<O>
    implements SealedSwitch<FileUploaderEvent, O> {
  O switchCase(FileUploaderEvent type) =>
      type is FileUploaderEventUpload ? onUpload(type) : onDefault(type);
  O onUpload(FileUploaderEventUpload upload);
  O onDefault(FileUploaderEvent fileUploaderEvent) =>
      throw UnimplementedError();
}
