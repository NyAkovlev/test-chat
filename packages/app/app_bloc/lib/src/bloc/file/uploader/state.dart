import 'package:domain/src/model/local_file/local_file_model.dart';
import 'package:equatable/equatable.dart';
import 'package:like_sealed/like_sealed.dart';
import 'package:util/util.dart';

part 'state.sealed.dart';

@likeSealed
abstract class FileUploaderState extends Equatable {
  @override
  List<Object> get props => [];
}

class FileUploaderStateInit extends FileUploaderState {}

class FileUploaderStateProgress extends FileUploaderState {}

class FileUploaderStateSuccess extends FileUploaderState with Unique {
  final LocalFileModel localFile;

  FileUploaderStateSuccess(this.localFile);
}

class FileUploaderStateError extends FileUploaderState with Unique {
  final dynamic error;

  FileUploaderStateError(this.error);
}
