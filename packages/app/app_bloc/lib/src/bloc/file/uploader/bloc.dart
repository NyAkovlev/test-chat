import 'package:domain/export.dart';
import 'package:domain_worker/domain_worker.dart';

import 'export.dart';
import 'package:app_bloc/export.dart';

class FileUploaderBloc extends BBloc<FileUploaderEvent, FileUploaderState>
    with FileUploaderEventSwitch<Stream<FileUploaderState>> {
  final FileWorker _fileCacheWorker;

  FileUploaderBloc(this._fileCacheWorker) : super(FileUploaderStateInit());

  @override
  Stream<FileUploaderState> onUpload(FileUploaderEventUpload event) async* {
    yield FileUploaderStateProgress();
    final file = await _fileCacheWorker.pickImage();
    if (file == null) {
      yield FileUploaderStateInit();
      return;
    }
    await event.onUpload?.call(file);
    yield FileUploaderStateSuccess(file);
  }

  @override
  Stream<FileUploaderState> mapErrorToState(error) async* {
    yield FileUploaderStateError(error);
  }
}
