import 'package:equatable/equatable.dart';
import 'package:like_sealed/like_sealed.dart';
import 'package:util/util.dart';

part 'event.sealed.dart';

@likeSealed
abstract class ErrorEvent  {}

class ErrorEventMessage extends ErrorEvent  {
  final dynamic error;

  ErrorEventMessage(this.error);
}
