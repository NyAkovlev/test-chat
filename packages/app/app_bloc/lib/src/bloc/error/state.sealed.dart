// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class ErrorStates {
  static ErrorStateInit init() {
    return ErrorStateInit();
  }

  static ErrorStateShow show(
    dynamic error,
  ) {
    return ErrorStateShow(error);
  }
}

abstract class ErrorStateSwitch<O> implements SealedSwitch<ErrorState, O> {
  O switchCase(ErrorState type) => type is ErrorStateInit
      ? onInit(type)
      : type is ErrorStateShow
          ? onShow(type)
          : onDefault(type);
  O onInit(ErrorStateInit init);
  O onShow(ErrorStateShow show);
  O onDefault(ErrorState errorState) => throw UnimplementedError();
}
