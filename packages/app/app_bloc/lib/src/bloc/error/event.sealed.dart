// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class ErrorEvents {
  static ErrorEventMessage message(
    dynamic error,
  ) {
    return ErrorEventMessage(error);
  }
}

abstract class ErrorEventSwitch<O> implements SealedSwitch<ErrorEvent, O> {
  O switchCase(ErrorEvent type) =>
      type is ErrorEventMessage ? onMessage(type) : onDefault(type);
  O onMessage(ErrorEventMessage message);
  O onDefault(ErrorEvent errorEvent) => throw UnimplementedError();
}
