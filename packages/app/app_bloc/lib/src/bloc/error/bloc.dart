import 'dart:async';
import 'package:app_bloc/export.dart';

class ErrorBloc extends BBloc<ErrorEvent, ErrorState>
    with OneSubBloc, ErrorEventSwitch<Stream<ErrorState>> {
  static ErrorBloc? _instance;

  ErrorBloc() : super(ErrorStateInit());

  @override
  Future<void> close() async {
    if (_instance == this) _instance = null;
    super.close();
  }

  static void error(dynamic errorToShow) {
    _instance?.add(ErrorEventMessage(errorToShow));
  }

  @override
  Stream<ErrorState> onMessage(ErrorEventMessage message) async* {
    yield ErrorStateShow(message.error);
  }

  @override
  Stream<ErrorState> mapErrorToState(error) async* {
    yield ErrorStateShow(error);
  }
}
