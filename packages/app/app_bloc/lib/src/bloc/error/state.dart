import 'package:equatable/equatable.dart';
import 'package:like_sealed/like_sealed.dart';
import 'package:util/util.dart';

part 'state.sealed.dart';

@likeSealed
abstract class ErrorState extends Equatable {
  @override
  List<Object> get props => [];
}

class ErrorStateInit extends ErrorState {}

class ErrorStateShow extends ErrorState with Unique {
  final dynamic error;

  ErrorStateShow(this.error);
}
