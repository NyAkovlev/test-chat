import 'package:app_bloc/export.dart';
import 'package:domain_worker/domain_worker.dart';

import 'export.dart';

class UserChangeAvatarBloc extends BBloc<UserChangeAvatarEvent, UserChangeAvatarState>
    with UserAvatarEventSwitch<Stream<UserChangeAvatarState>> {
  final UserEditionWorker _worker;

  UserChangeAvatarBloc(this._worker) : super(UserChangeAvatarStateInit());

  @override
  Stream<UserChangeAvatarState> mapErrorToState(error) async* {
    yield UserChangeAvatarStateError(error);
  }

  @override
  Stream<UserChangeAvatarState> onSet(UserChangeAvatarEventSet set) async* {
    yield UserChangeAvatarStateProgress();
    await _worker.changeAvatar();
    yield UserChangeAvatarStateSuccess();
  }
}
