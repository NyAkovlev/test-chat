// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class UserAvatarStates {
  static UserChangeAvatarStateInit init() {
    return UserChangeAvatarStateInit();
  }

  static UserChangeAvatarStateProgress progress() {
    return UserChangeAvatarStateProgress();
  }

  static UserChangeAvatarStateSuccess success() {
    return UserChangeAvatarStateSuccess();
  }

  static UserChangeAvatarStateError error(
    dynamic error,
  ) {
    return UserChangeAvatarStateError(error);
  }
}

abstract class UserAvatarStateSwitch<O>
    implements SealedSwitch<UserChangeAvatarState, O> {
  O switchCase(UserChangeAvatarState type) => type is UserChangeAvatarStateInit
      ? onInit(type)
      : type is UserChangeAvatarStateProgress
          ? onProgress(type)
          : type is UserChangeAvatarStateSuccess
              ? onSuccess(type)
              : type is UserChangeAvatarStateError
                  ? onError(type)
                  : onDefault(type);
  O onInit(UserChangeAvatarStateInit init);
  O onProgress(UserChangeAvatarStateProgress progress);
  O onSuccess(UserChangeAvatarStateSuccess success);
  O onError(UserChangeAvatarStateError error);
  O onDefault(UserChangeAvatarState userAvatarState) => throw UnimplementedError();
}
