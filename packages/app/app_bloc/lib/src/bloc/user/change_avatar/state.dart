import 'package:equatable/equatable.dart';
import 'package:like_sealed/like_sealed.dart';
import 'package:util/util.dart';

part 'state.sealed.dart';

@likeSealed
abstract class UserChangeAvatarState extends Equatable {
  @override
  List<Object> get props => [];
}

class UserChangeAvatarStateInit extends UserChangeAvatarState {}

class UserChangeAvatarStateProgress extends UserChangeAvatarState {}

class UserChangeAvatarStateSuccess extends UserChangeAvatarState {}

class UserChangeAvatarStateError extends UserChangeAvatarState with Unique {
  final dynamic error;

  UserChangeAvatarStateError(this.error);
}
