// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class UserAvatarEvents {
  static UserChangeAvatarEventSet set() {
    return UserChangeAvatarEventSet();
  }
}

abstract class UserAvatarEventSwitch<O>
    implements SealedSwitch<UserChangeAvatarEvent, O> {
  O switchCase(UserChangeAvatarEvent type) =>
      type is UserChangeAvatarEventSet ? onSet(type) : onDefault(type);
  O onSet(UserChangeAvatarEventSet set);
  O onDefault(UserChangeAvatarEvent userAvatarEvent) => throw UnimplementedError();
}
