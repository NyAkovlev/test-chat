import 'package:app_bloc/export.dart';
import 'package:domain/export.dart';
import 'package:domain_worker/domain_worker.dart';

class UserPaginationBloc extends PaginationBloc<UserModel> {
  final UserWorker _userWorker;
  final String _search;

  UserPaginationBloc(
    this._search,
    this._userWorker,
  ) : super(pageSize: 10);

  @override
  Future<ListResponse<UserModel>> findItems(int pageSize, int page) {
    return _userWorker.findUser(
      _search,
      page,
      pageSize,
    );
  }
}
