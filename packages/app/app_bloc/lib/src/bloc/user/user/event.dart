import 'package:domain/export.dart';
import 'package:equatable/equatable.dart';
import 'package:like_sealed/like_sealed.dart';
import 'package:util/util.dart';

part 'event.sealed.dart';

@likeSealed
abstract class UserEvent  {}

class UserEventSync extends UserEvent {}

class UserEventUpdate extends UserEvent {
  final UserModel? userModel;

  UserEventUpdate(this.userModel);
}
