import 'package:app_bloc/export.dart';
import 'package:domain/export.dart';
import 'package:domain_worker/domain_worker.dart';

import 'export.dart';

class UserBloc extends BBloc<UserEvent, UserState>
    with UserEventSwitch<Stream<UserState>> {
  final MyUserWorker _userWorker;
  final UserPref _userPref;

  UserBloc(
    this._userPref,
    this._userWorker,
  ) : super(UserState(_userWorker.getUser())) {
    _userPref.user.addListeners(_onUser);
    if (state.user != null) {
      add(UserEventSync());
    }
  }

  _onUser(UserModel? old, UserModel? current) {
    add(UserEventUpdate(current));
  }

  @override
  Future<void> close() {
    _userPref.user.removeListener(_onUser);
    return super.close();
  }

  @override
  Stream<UserState> onSync(UserEventSync sync) async* {
    try {
      await _userWorker.syncUser();
    } catch (_) {
      //ignore
    }
  }

  @override
  Stream<UserState> onUpdate(UserEventUpdate update) async* {
    yield UserState(_userWorker.getUser());
  }

  @override
  Stream<UserState> mapErrorToState(error) async* {
    ErrorBloc.error(error);
    yield UserState(_userWorker.getUser());
  }
}
