// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class UserStates {
  static UserState(
    UserModel? user,
  ) {
    return UserState(user);
  }
}

abstract class UserStateSwitch<O> implements SealedSwitch<UserState, O> {
  O switchCase(UserState type) => onDefault(type);
  O onDefault(UserState userState) => throw UnimplementedError();
}
