// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class UserEvents {
  static UserEventSync sync() {
    return UserEventSync();
  }

  static UserEventUpdate update(
    UserModel? userModel,
  ) {
    return UserEventUpdate(userModel);
  }
}

abstract class UserEventSwitch<O> implements SealedSwitch<UserEvent, O> {
  O switchCase(UserEvent type) => type is UserEventSync
      ? onSync(type)
      : type is UserEventUpdate
          ? onUpdate(type)
          : onDefault(type);
  O onSync(UserEventSync sync);
  O onUpdate(UserEventUpdate update);
  O onDefault(UserEvent userEvent) => throw UnimplementedError();
}
