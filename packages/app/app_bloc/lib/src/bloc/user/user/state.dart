import 'package:domain/export.dart';
import 'package:equatable/equatable.dart';
import 'package:like_sealed/like_sealed.dart';

part 'state.sealed.dart';

@likeSealed
class UserState extends Equatable {
  final UserModel? user;

  UserState(this.user);

  @override
  List<Object?> get props => [user];
}
