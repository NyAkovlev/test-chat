import 'package:app_bloc/export.dart';
import 'package:domain/export.dart';

import 'export.dart';

class ChatCreationBloc extends BBloc<ChatCreationEvent, ChatCreationState>
    with ChatCreationEventSwitch<Stream<ChatCreationState>> {
  final ChatApi _chatApi;

  ChatCreationBloc(this._chatApi) : super(ChatCreationStateInit());

  @override
  Stream<ChatCreationState> onPersonalCreate(
      ChatCreationEventPersonalCreate event) async* {
    yield ChatCreationStateProgress();
    final existChat = await _chatApi.personalChat(event.userModel.id);
    if (existChat != null) {
      yield ChatCreationStateSuccess(existChat);
      return;
    }
    final chatModel = await _chatApi.createPersonalChat(event.userModel.id);
    yield ChatCreationStateSuccess(chatModel);
  }

  @override
  Stream<ChatCreationState> mapErrorToState(error) async* {
    yield ChatCreationStateError(error);
  }
}
