import 'package:domain/export.dart';
import 'package:equatable/equatable.dart';
import 'package:like_sealed/like_sealed.dart';
import 'package:util/util.dart';

part 'state.sealed.dart';

@likeSealed
abstract class ChatCreationState extends Equatable {
  @override
  List<Object> get props => [];
}

class ChatCreationStateInit extends ChatCreationState {}

class ChatCreationStateProgress extends ChatCreationState {}

class ChatCreationStateError extends ChatCreationState {
  final dynamic error;

  ChatCreationStateError(this.error);

  @override
  List<Object> get props => [error];
}

class ChatCreationStateSuccess extends ChatCreationState with Unique {
  final ChatModel chatModel;

  ChatCreationStateSuccess(this.chatModel);
}
