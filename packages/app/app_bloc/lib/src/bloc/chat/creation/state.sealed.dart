// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class ChatCreationStates {
  static ChatCreationStateInit init() {
    return ChatCreationStateInit();
  }

  static ChatCreationStateProgress progress() {
    return ChatCreationStateProgress();
  }

  static ChatCreationStateError error(
    dynamic error,
  ) {
    return ChatCreationStateError(error);
  }

  static ChatCreationStateSuccess success(
    ChatModel chatModel,
  ) {
    return ChatCreationStateSuccess(chatModel);
  }
}

abstract class ChatCreationStateSwitch<O>
    implements SealedSwitch<ChatCreationState, O> {
  O switchCase(ChatCreationState type) => type is ChatCreationStateInit
      ? onInit(type)
      : type is ChatCreationStateProgress
          ? onProgress(type)
          : type is ChatCreationStateError
              ? onError(type)
              : type is ChatCreationStateSuccess
                  ? onSuccess(type)
                  : onDefault(type);
  O onInit(ChatCreationStateInit init);
  O onProgress(ChatCreationStateProgress progress);
  O onError(ChatCreationStateError error);
  O onSuccess(ChatCreationStateSuccess success);
  O onDefault(ChatCreationState chatCreationState) =>
      throw UnimplementedError();
}
