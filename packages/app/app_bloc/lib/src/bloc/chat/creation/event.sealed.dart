// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class ChatCreationEvents {
  static ChatCreationEventPersonalCreate personalCreate(
    UserModel userModel,
  ) {
    return ChatCreationEventPersonalCreate(userModel);
  }
}

abstract class ChatCreationEventSwitch<O>
    implements SealedSwitch<ChatCreationEvent, O> {
  O switchCase(ChatCreationEvent type) =>
      type is ChatCreationEventPersonalCreate
          ? onPersonalCreate(type)
          : onDefault(type);
  O onPersonalCreate(ChatCreationEventPersonalCreate personalCreate);
  O onDefault(ChatCreationEvent chatCreationEvent) =>
      throw UnimplementedError();
}
