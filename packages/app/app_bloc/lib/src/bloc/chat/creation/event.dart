import 'package:domain/export.dart';
import 'package:like_sealed/like_sealed.dart';
import 'package:util/util.dart';

part 'event.sealed.dart';

@likeSealed
abstract class ChatCreationEvent {}

class ChatCreationEventPersonalCreate extends ChatCreationEvent with Unique {
  final UserModel userModel;

  ChatCreationEventPersonalCreate(this.userModel);
}
