import 'package:like_sealed/like_sealed.dart';

part 'event.sealed.dart';

@likeSealed
abstract class ChatAvatarEvent {}

class ChatAvatarEventSet extends ChatAvatarEvent {}
