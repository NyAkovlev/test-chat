import 'package:app_bloc/export.dart';
import 'package:domain/export.dart';
import 'package:domain_worker/domain_worker.dart';

import 'export.dart';

class ChatAvatarBloc extends BBloc<ChatAvatarEvent, ChatAvatarState>
    with ChatAvatarEventSwitch<Stream<ChatAvatarState>> {
  final ChatEditionWorker _worker;
  final ChatModel chatModel;

  ChatAvatarBloc(this._worker, this.chatModel) : super(ChatAvatarStateInit());

  @override
  Stream<ChatAvatarState> mapErrorToState(error) async* {
    yield ChatAvatarStateError(error);
  }

  @override
  Stream<ChatAvatarState> onSet(ChatAvatarEventSet set) async* {
    yield ChatAvatarStateProgress();
    await _worker.changeAvatar(chatModel);
    yield ChatAvatarStateSuccess();
  }
}
