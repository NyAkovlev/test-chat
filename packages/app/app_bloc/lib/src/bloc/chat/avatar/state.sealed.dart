// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class ChatAvatarStates {
  static ChatAvatarStateInit init() {
    return ChatAvatarStateInit();
  }

  static ChatAvatarStateProgress progress() {
    return ChatAvatarStateProgress();
  }

  static ChatAvatarStateSuccess success() {
    return ChatAvatarStateSuccess();
  }

  static ChatAvatarStateError error(
    dynamic error,
  ) {
    return ChatAvatarStateError(error);
  }
}

abstract class ChatAvatarStateSwitch<O>
    implements SealedSwitch<ChatAvatarState, O> {
  O switchCase(ChatAvatarState type) => type is ChatAvatarStateInit
      ? onInit(type)
      : type is ChatAvatarStateProgress
          ? onProgress(type)
          : type is ChatAvatarStateSuccess
              ? onSuccess(type)
              : type is ChatAvatarStateError
                  ? onError(type)
                  : onDefault(type);
  O onInit(ChatAvatarStateInit init);
  O onProgress(ChatAvatarStateProgress progress);
  O onSuccess(ChatAvatarStateSuccess success);
  O onError(ChatAvatarStateError error);
  O onDefault(ChatAvatarState chatAvatarState) => throw UnimplementedError();
}
