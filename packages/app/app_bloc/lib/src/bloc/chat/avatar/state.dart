import 'package:equatable/equatable.dart';
import 'package:like_sealed/like_sealed.dart';
import 'package:util/util.dart';

part 'state.sealed.dart';

@likeSealed
abstract class ChatAvatarState extends Equatable {
  @override
  List<Object> get props => [];
}

class ChatAvatarStateInit extends ChatAvatarState {}

class ChatAvatarStateProgress extends ChatAvatarState {}

class ChatAvatarStateSuccess extends ChatAvatarState {}

class ChatAvatarStateError extends ChatAvatarState with Unique {
  final dynamic error;

  ChatAvatarStateError(this.error);
}
