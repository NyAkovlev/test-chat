// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class ChatAvatarEvents {
  static ChatAvatarEventSet set() {
    return ChatAvatarEventSet();
  }
}

abstract class ChatAvatarEventSwitch<O>
    implements SealedSwitch<ChatAvatarEvent, O> {
  O switchCase(ChatAvatarEvent type) =>
      type is ChatAvatarEventSet ? onSet(type) : onDefault(type);
  O onSet(ChatAvatarEventSet set);
  O onDefault(ChatAvatarEvent chatAvatarEvent) => throw UnimplementedError();
}
