import 'package:app_bloc/export.dart';
import 'package:domain/export.dart';
import 'package:domain_worker/domain_worker.dart';

class ChatPaginationBloc extends PaginationBloc<ChatModel> {
  final ChatWorker _chatWorker;

  ChatPaginationBloc(
    this._chatWorker,
  ) : super(pageSize: 10);

  @override
  Future<ListResponse<ChatModel>> findItems(int pageSize, int page) {
    return _chatWorker.getChats(
      page,
      pageSize,
    );
  }
}
