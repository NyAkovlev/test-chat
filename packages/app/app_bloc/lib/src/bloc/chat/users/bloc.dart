import 'export.dart';
import 'package:app_bloc/export.dart';

class ChatUsersBloc extends BBloc<ChatUsersEvent, ChatUsersState>
    with ChatUsersEventSwitch<Stream<ChatUsersState>> {
  ChatUsersBloc() : super(ChatUsersStateInit());

  @override
  Stream<ChatUsersState> mapErrorToState(error) {
    throw UnimplementedError();
  }
}
