// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class TemplateStates {
  static ChatUsersStateInit init() {
    return ChatUsersStateInit();
  }
}

abstract class TemplateStateSwitch<O>
    implements SealedSwitch<ChatUsersState, O> {
  O switchCase(ChatUsersState type) =>
      type is ChatUsersStateInit ? onInit(type) : onDefault(type);
  O onInit(ChatUsersStateInit init);
  O onDefault(ChatUsersState templateState) => throw UnimplementedError();
}
