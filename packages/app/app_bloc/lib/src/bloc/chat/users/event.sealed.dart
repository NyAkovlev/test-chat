// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class TemplateEvents {}

abstract class ChatUsersEventSwitch<O>
    implements SealedSwitch<ChatUsersEvent, O> {
  O switchCase(ChatUsersEvent type) => onDefault(type);
  O onDefault(ChatUsersEvent templateEvent) => throw UnimplementedError();
}
