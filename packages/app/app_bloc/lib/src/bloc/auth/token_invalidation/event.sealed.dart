// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class InvalidationTokenEvents {
  static InvalidationTokenEventInvalid invalid() {
    return InvalidationTokenEventInvalid();
  }
}

abstract class InvalidationTokenEventSwitch<O>
    implements SealedSwitch<InvalidationTokenEvent, O> {
  O switchCase(InvalidationTokenEvent type) =>
      type is InvalidationTokenEventInvalid ? onInvalid(type) : onDefault(type);
  O onInvalid(InvalidationTokenEventInvalid invalid);
  O onDefault(InvalidationTokenEvent invalidationTokenEvent) =>
      throw UnimplementedError();
}
