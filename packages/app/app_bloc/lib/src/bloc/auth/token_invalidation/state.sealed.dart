// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class InvalidationTokenStates {
  static InvalidationTokenStateInit init() {
    return InvalidationTokenStateInit();
  }

  static InvalidationTokenStateInvalidated invalidated() {
    return InvalidationTokenStateInvalidated();
  }
}

abstract class InvalidationTokenStateSwitch<O>
    implements SealedSwitch<InvalidationTokenState, O> {
  O switchCase(InvalidationTokenState type) =>
      type is InvalidationTokenStateInit
          ? onInit(type)
          : type is InvalidationTokenStateInvalidated
              ? onInvalidated(type)
              : onDefault(type);
  O onInit(InvalidationTokenStateInit init);
  O onInvalidated(InvalidationTokenStateInvalidated invalidated);
  O onDefault(InvalidationTokenState invalidationTokenState) =>
      throw UnimplementedError();
}
