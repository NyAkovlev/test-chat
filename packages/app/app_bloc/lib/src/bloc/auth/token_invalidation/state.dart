import 'package:equatable/equatable.dart';
import 'package:like_sealed/like_sealed.dart';

part 'state.sealed.dart';

@likeSealed
abstract class InvalidationTokenState extends Equatable {
  @override
  List<Object> get props => [];
}

class InvalidationTokenStateInit extends InvalidationTokenState {}

class InvalidationTokenStateInvalidated extends InvalidationTokenState {}
