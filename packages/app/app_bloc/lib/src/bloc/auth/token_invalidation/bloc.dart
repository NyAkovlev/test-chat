import 'package:domain/export.dart';

import 'export.dart';
import 'package:app_bloc/export.dart';

class InvalidationTokenBloc
    extends BBloc<InvalidationTokenEvent, InvalidationTokenState>
    with InvalidationTokenEventSwitch<Stream<InvalidationTokenState>> {
  final TokenInvalidateListener _tokenInvalidateListener;
  final UserPref _userPref;
  final AuthPref _authPref;

  InvalidationTokenBloc(
    this._tokenInvalidateListener,
    this._userPref,
    this._authPref,
  ) : super(InvalidationTokenStateInit()) {
    _tokenInvalidateListener.addListener(_onInvalidToken);
  }

  _onInvalidToken() {
    _userPref.clear();
    _authPref.clear();
    add(InvalidationTokenEventInvalid());
  }

  @override
  Stream<InvalidationTokenState> onInvalid(
      InvalidationTokenEventInvalid invalidate) async* {
    yield InvalidationTokenStateInvalidated();
  }

  @override
  Stream<InvalidationTokenState> mapErrorToState(error) async* {
    ErrorBloc.error(error);
    yield InvalidationTokenStateInit();
  }
}
