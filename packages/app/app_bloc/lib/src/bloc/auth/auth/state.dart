import 'package:equatable/equatable.dart';
import 'package:like_sealed/like_sealed.dart';
import 'package:util/util.dart';

part 'state.sealed.dart';

@likeSealed
abstract class AuthState extends Equatable {
  @override
  List<Object> get props => [];
}

class AuthStateInit extends AuthState {}

class AuthStateProgress extends AuthState {}

class AuthStateSuccess extends AuthState {}

class AuthStateError extends AuthState with Unique {
  final dynamic error;

  AuthStateError(this.error);
}
