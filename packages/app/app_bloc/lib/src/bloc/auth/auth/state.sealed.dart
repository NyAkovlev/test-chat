// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class AuthStates {
  static AuthStateInit init() {
    return AuthStateInit();
  }

  static AuthStateProgress progress() {
    return AuthStateProgress();
  }

  static AuthStateSuccess success() {
    return AuthStateSuccess();
  }

  static AuthStateError error(
    dynamic error,
  ) {
    return AuthStateError(error);
  }
}

abstract class AuthStateSwitch<O> implements SealedSwitch<AuthState, O> {
  O switchCase(AuthState type) => type is AuthStateInit
      ? onInit(type)
      : type is AuthStateProgress
          ? onProgress(type)
          : type is AuthStateSuccess
              ? onSuccess(type)
              : type is AuthStateError
                  ? onError(type)
                  : onDefault(type);
  O onInit(AuthStateInit init);
  O onProgress(AuthStateProgress progress);
  O onSuccess(AuthStateSuccess success);
  O onError(AuthStateError error);
  O onDefault(AuthState authState) => throw UnimplementedError();
}
