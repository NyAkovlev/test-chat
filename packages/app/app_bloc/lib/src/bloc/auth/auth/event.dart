import 'package:equatable/equatable.dart';
import 'package:like_sealed/like_sealed.dart';
import 'package:util/util.dart';

part 'event.sealed.dart';

@likeSealed
abstract class AuthEvent  {}

class AuthEventSignInByLogin extends AuthEvent {
  final String login;
  final String password;

  AuthEventSignInByLogin(this.login, this.password);
}
class AuthEventSignUpByLogin extends AuthEvent {
  final String login;
  final String password;

  AuthEventSignUpByLogin(this.login, this.password);
}
