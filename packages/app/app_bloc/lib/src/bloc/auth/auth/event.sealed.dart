// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class AuthEvents {
  static AuthEventSignInByLogin signInByLogin(
    String login,
    String password,
  ) {
    return AuthEventSignInByLogin(login, password);
  }

  static AuthEventSignUpByLogin signUpByLogin(
    String login,
    String password,
  ) {
    return AuthEventSignUpByLogin(login, password);
  }
}

abstract class AuthEventSwitch<O> implements SealedSwitch<AuthEvent, O> {
  O switchCase(AuthEvent type) => type is AuthEventSignInByLogin
      ? onSignInByLogin(type)
      : type is AuthEventSignUpByLogin
          ? onSignUpByLogin(type)
          : onDefault(type);
  O onSignInByLogin(AuthEventSignInByLogin signInByLogin);
  O onSignUpByLogin(AuthEventSignUpByLogin signUpByLogin);
  O onDefault(AuthEvent authEvent) => throw UnimplementedError();
}
