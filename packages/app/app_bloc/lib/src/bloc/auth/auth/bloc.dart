import 'export.dart';
import 'package:app_bloc/export.dart';
import 'package:domain_worker/domain_worker.dart';

class AuthBloc extends BBloc<AuthEvent, AuthState>
    with AuthEventSwitch<Stream<AuthState>> {
  final AuthWorker _authWorker;
  final MyUserWorker _userWorker;

  AuthBloc(this._authWorker, this._userWorker) : super(AuthStateInit());

  @override
  Stream<AuthState> mapErrorToState(error) async* {
    yield AuthStateError(error);
  }

  @override
  Stream<AuthState> onSignInByLogin(AuthEventSignInByLogin event) async* {
    yield AuthStates.progress();
    await _authWorker.signIn(event.login, event.password);
    await _userWorker.syncUser();
    yield AuthStates.success();
  }

  @override
  Stream<AuthState> onSignUpByLogin(AuthEventSignUpByLogin event) async* {
    yield AuthStates.progress();
    await _authWorker.signUp(event.login, event.password);
    await _userWorker.syncUser();
    yield AuthStates.success();
  }
}
