extension Let<I> on I {
  O let<O>(O Function(I) fun) {
    return fun(this);
  }
}
