extension IterableUtil<T> on Iterable<T> {
  T? get lastOrNull => this.isEmpty ? null : this.last;

  List<T> separate(T Function(int) builder) {
    final out = <T>[];
    var i = 0;
    for (var value in this) {
      out.add(value);
      if (i != this.length - 1) {
        out.add(builder(i));
      }
      i++;
    }
    return out;
  }

  List<O> indexMap<O>(O Function(T, int) builder) {
    final out = <O>[];
    var i = 0;
    for (var value in this) {
      out.add(builder(value, i));
      i++;
    }
    return out;
  }
}
