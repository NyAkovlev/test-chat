import 'dart:async';

extension CompleterUtil<T> on Completer<T> {
  void tryComplete([FutureOr<T>? value]) {
    try {
      this.complete(value);
    } catch (ignore) {}
  }
}
