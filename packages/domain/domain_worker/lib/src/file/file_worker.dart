import 'package:domain/export.dart';

class FileWorker {
  final FileNetworkApi _fileApi;
  final CachedFileApi _cachedFileApi;
  final FilePickerApi _filePickerApi;
  final SecurityGenerator _securityGenerator;

  FileWorker(this._fileApi, this._cachedFileApi, this._filePickerApi,
      this._securityGenerator);

  Future<LocalFileModel?> pickImage() async {
    final path = await _filePickerApi.pickImage();
    if (path == null) {
      return null;
    }
    final uuid = await uploadFile(path);
    return getFile(uuid);
  }

  Future<String> uploadFile(String path) async {
    final uuid = _securityGenerator.uuidV4();
    await _fileApi.uploadFile(path, uuid);
    await _cachedFileApi.savePath(uuid, path);
    return uuid;
  }

  Future<LocalFileModel> getFile(String uuid) async {
    var localFile = await _cachedFileApi.get(uuid);
    if (localFile != null) {
      return localFile;
    } else {
      final data = await _fileApi.downloadFile(uuid);
      await _cachedFileApi.saveData(uuid, data);
      return LocalFileModel.data(uuid, data);
    }
  }
}
