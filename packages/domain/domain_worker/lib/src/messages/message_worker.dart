import 'dart:async';

import 'package:domain/export.dart';

class MessageWorker {
  final MessageApi _messageApi;

  MessageWorker(this._messageApi);

  StreamSubscription<SocketMessageState> messageStream({
    required MessageSocketController controller,
    required void Function(MessageModel) onAdd,
  }) {
    return controller.messageStream().listen((v) {
      if (v.name == SocketMessageState.added) {
        onAdd(v.model);
      }
    });
  }

  Future<MessageModel> sendMessage({
    required MessageContentModel message,
    required ChatModel chatModel,
  }) {
    return _messageApi.sendMessage(chatModel.id, message);
  }

  Future<ListResponse<MessageModel>> messagesOfChatBefore(
    int chatId,
    DateTime before,
    int pageSize,
  ) {
    return _messageApi.messagesOfChatBefore(
      chatId,
      before,
      pageSize,
    );
  }
}
