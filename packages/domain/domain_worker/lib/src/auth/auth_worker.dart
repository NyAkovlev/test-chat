import 'package:domain/export.dart';

class AuthWorker {
  final DevicePref _devicePref;
  final DeviceApi _deviceApi;
  final DeviceInfoApi _deviceInfoApi;
  final AuthApi _authApi;
  final AuthPref _authPref;
  final SecurityGenerator _securityGenerator;

  AuthWorker(
    this._devicePref,
    this._deviceApi,
    this._deviceInfoApi,
    this._securityGenerator,
    this._authApi,
    this._authPref,
  );

  Future _createDevice() async {
    final name = await _deviceInfoApi.deviceName();
    final secret = _securityGenerator.uuidV4();
    final device = await _deviceApi.createDevice(name, secret);

    await _devicePref.deviceId.set(device.id);
    await _devicePref.deviceName.set(name);
    await _devicePref.deviceSecret.set(secret);
  }

  Future<DeviceModel> _getDevice() async {
    if (_devicePref.deviceId.get() == null) {
      await _createDevice();
    }
    return DeviceModel(
      _devicePref.deviceId.get()!,
      _devicePref.deviceName.get()!,
      _devicePref.deviceSecret.get()!,
    );
  }

  Future _saveToken(AuthResponse response) async {
    await _authPref.accessToken.set(response.accessToken);
    await _authPref.refreshToken.set(response.refreshToken);
  }

  Future signIn(String username, String password) async {
    final device = await _getDevice();
    final response = await _authApi.signIn(
      device.secret!,
      device.id,
      username,
      password,
    );
    await _saveToken(response);
  }

  Future signUp(String username, String password) async {
    final device = await _getDevice();
    final response = await _authApi.signUp(
      device.secret!,
      device.id,
      username,
      password,
    );
    await _saveToken(response);
  }

  Future refreshToken() async {
    final device = await _getDevice();
    final response = await _authApi.refreshToken(
      device.secret!,
      device.id,
      _authPref.refreshToken.get()!,
    );
    await _saveToken(response);
  }
}
