import 'package:domain/export.dart';
import 'package:domain_worker/domain_worker.dart';

class ChatEditionWorker {
  final ChatApi _chatApi;
  final FileWorker _fileWorker;

  ChatEditionWorker(this._chatApi, this._fileWorker);

  Future<ChatModel?> changeAvatar(ChatModel chatModel) async {
    final image = await _fileWorker.pickImage();
    if (image != null) {
      final chat = await _chatApi.changeAvatar(chatModel.id, image.id);
      return chat;
    }
    return null;
  }
}
