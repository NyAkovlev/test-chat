import 'package:domain/export.dart';

class ChatUserWorker {
  final ChatApi _chatApi;
  List<ChatUserRole>? _roles;

  ChatUserWorker(this._chatApi);

  Future initUsersRole() async {
    _roles = await _chatApi.chatRole();
  }

  Future<ListResponse<ChatUserModel>> getUsersOfBoard(
    int page,
    int chatId,
  ) {
    return _chatApi.memberOfChat(_size, page, chatId);
  }

  static final _size = 10;
}
