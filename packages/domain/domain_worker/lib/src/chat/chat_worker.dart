import 'package:domain/export.dart';

class ChatWorker {
  final ChatApi _chatApi;

  ChatWorker(this._chatApi);

  Future<ListResponse<ChatModel>> getChats(int page, int pageSize) async {
    final response = await _chatApi.chats(pageSize, page);

    return response;
  }

  Future<ChatModel> createPersonalChat(UserModel secondUser) async {
    final response = await _chatApi.createPersonalChat(secondUser.id);

    return response;
  }
}
