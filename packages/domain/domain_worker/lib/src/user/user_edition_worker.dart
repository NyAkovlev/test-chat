import 'package:domain/export.dart';
import 'package:domain_worker/domain_worker.dart';

class UserEditionWorker {
  final UserApi _userApi;
  final UserPref _userPref;
  final FileWorker _fileWorker;

  UserEditionWorker(
    this._userApi,
    this._userPref,
    this._fileWorker,
  );

  Future changeAvatar() async {
    final image = await _fileWorker.pickImage();
    if (image != null) {
      final user = await _userApi.changeAvatar(image.id);
      await _userPref.user.set(user);
    }
  }
}
