import 'package:domain/export.dart';

class MyUserWorker {
  final UserApi _userApi;
  final UserPref _userPref;

  MyUserWorker(this._userApi, this._userPref);

  UserModel? getUser() {
    return _userPref.user.get();
  }

  Future<UserModel> syncUser() async {
    final user = await _userApi.getUser();
    await _userPref.user.set(user);
    return user;
  }
}
