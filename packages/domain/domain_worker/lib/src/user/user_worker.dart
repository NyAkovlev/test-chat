import 'package:domain/export.dart';

class UserWorker {
  final UserApi _userApi;

  UserWorker(this._userApi);

  Future<ListResponse<UserModel>> findUser(
    String username,
    int page,
    int pageSize,
  ) {
    return _userApi.findUser(username, pageSize, page);
  }
}
