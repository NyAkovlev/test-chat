import 'package:domain/export.dart';
import 'package:path_provider/path_provider.dart';

class DirectoryPathApiPlatform extends DirectoryPathApi {
  @override
  Future<String> tempDir() {
    return getTemporaryDirectory().then((v) => v.path);
  }
}
