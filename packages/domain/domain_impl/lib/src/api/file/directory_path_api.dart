import 'package:domain/export.dart';

import 'directory_path_api_platform/directory_path_api_platform.dart'
    if (dart.library.io) "directory_path_api_platform/directory_path_api_platform_io.dart"
    if (dart.library.js) "directory_path_api_platform/directory_path_api_platform_web.dart";

class DirectoryPathApiImpl extends DirectoryPathApi {
  final directoryPathApiPlatform = DirectoryPathApiPlatform();

  DirectoryPathApiImpl();

  @override
  Future<String> tempDir() {
    return directoryPathApiPlatform.tempDir();
  }
}
