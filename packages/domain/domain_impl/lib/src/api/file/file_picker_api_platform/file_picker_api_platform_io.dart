import 'dart:io';
import 'package:domain/export.dart';
import 'package:image_picker/image_picker.dart';

class FilePickerApiPlatform extends FilePickerApi {
  final imagePicker = ImagePicker();

  @override
  Future<String?> pickImage() async {
    if (Platform.isAndroid || Platform.isIOS) {
      return imagePicker.getImage(source: ImageSource.gallery).then((v) {
        return v?.path;
      });
    }
    throw UnimplementedError();
  }
}
