import 'package:domain/export.dart';

import 'file_picker_api_platform/file_picker_api_platform.dart'
    if (dart.library.io) "file_picker_api_platform/file_picker_api_platform_io.dart"
    if (dart.library.js) "file_picker_api_platform/file_picker_api_platform_web.dart";

class FilePickerApiImpl extends FilePickerApi {
  FilePickerApiPlatform _platform = FilePickerApiPlatform();

  Future<String?> pickImage() {
    return _platform.pickImage();
  }
}
