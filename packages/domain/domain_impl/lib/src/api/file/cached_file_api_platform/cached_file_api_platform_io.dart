import 'dart:io';
import 'dart:typed_data';

import 'package:domain/export.dart';

class CachedFilePlatformApi extends CachedFileApi {
  final Directory directory;

  CachedFilePlatformApi(String tempDir) : directory = Directory(tempDir);

  Future<LocalFileModel?> get(String id) async {
    final file = File(directory.path + Platform.pathSeparator + id);
    if (await file.exists()) {
      return LocalFileModel(
        id,
        () => file.readAsBytes(),
      );
    } else {
      return null;
    }
  }

  Future savePath(String id, String path) async {
    final file = File(directory.path + Platform.pathSeparator + id);

    if (await file.exists()) {
      await file.delete();
    }
    await File(path).copy(file.path);
  }

  @override
  Future<void> saveData(String id, Uint8List data) async {
    final file = File(directory.path + Platform.pathSeparator + id);

    if (await file.exists()) {
      await file.delete();
    }
    await file.create(recursive: true);
    await file.writeAsBytes(data);
  }
}
