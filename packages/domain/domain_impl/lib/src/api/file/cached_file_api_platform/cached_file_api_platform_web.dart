import 'dart:typed_data';

import 'package:domain/export.dart';

class CachedFilePlatformApi extends CachedFileApi {
  CachedFilePlatformApi(String tempDir);

  Future<LocalFileModel?> get(String name) async {
    throw UnimplementedError();
  }

  Future savePath(String name, String path) async {
    throw UnimplementedError();
  }

  @override
  Future<void> saveData(String name, Uint8List data) async {
    throw UnimplementedError();
  }
}
