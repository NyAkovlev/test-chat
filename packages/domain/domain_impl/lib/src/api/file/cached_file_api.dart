import 'dart:typed_data';

import 'package:domain/export.dart';

import 'cached_file_api_platform/cached_file_api_platform.dart'
    if (dart.library.io) "cached_file_api_platform/cached_file_api_platform_io.dart"
    if (dart.library.js) "cached_file_api_platform/cached_file_api_platform_web.dart";

class CachedFileApiImpl implements CachedFileApi {
  final CachedFilePlatformApi cachedFilePlatformApi;

  CachedFileApiImpl(String tempDir)
      : cachedFilePlatformApi = CachedFilePlatformApi(tempDir);

  Future<LocalFileModel?> get(String name) async {
    return cachedFilePlatformApi.get(name);
  }

  Future savePath(String name, String path) async {
    return cachedFilePlatformApi.savePath(name, path);
  }

  @override
  Future<void> saveData(String name, Uint8List data) {
    return cachedFilePlatformApi.saveData(name, data);
  }
}
