import 'dart:io';

import 'package:device_info/device_info.dart';

Future<String> getDeviceName(DeviceInfoPlugin deviceInfo) async {
  if (Platform.isAndroid) {
    final info = await deviceInfo.androidInfo;
    return info.model;
  } else if (Platform.isIOS) {
    final info = await deviceInfo.iosInfo;
    return info.utsname.machine;
  } else if (Platform.isWindows) {
    return "windows";
  }
  throw UnimplementedError();
}
