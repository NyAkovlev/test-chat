import 'package:device_info/device_info.dart';
import 'package:domain/export.dart';
import 'device_name/device_name.dart'
    if (dart.library.io) "device_name/device_name_io.dart"
    if (dart.library.js) "device_name/device_name_web.dart";

class DeviceInfoApiImpl extends DeviceInfoApi {
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

  Future<String> deviceName() async {
    return getDeviceName(deviceInfo);
  }
}
