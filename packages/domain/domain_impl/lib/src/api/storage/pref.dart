import 'dart:convert';

import 'package:domain/export.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class SharedPref<T> extends Pref<T> {
  final SharedPreferences sharedPreferences;

  SharedPref(this.sharedPreferences, String key) : super(key);

  @override
  Future set(T? value) {
    final old = get();
    notifyListeners(old, value);
    return onSet(value);
  }

  Future onSet(T? value) {
    if (value == null) {
      return sharedPreferences.remove(key);
    } else {
      return _onSetValue(value);
    }
  }

  Future _onSetValue(T value);
}

class StringPref extends SharedPref<String> {
  StringPref(SharedPreferences sharedPreferences, String key)
      : super(sharedPreferences, key);

  @override
  String? get() {
    return sharedPreferences.getString(key);
  }

  @override
  Future _onSetValue(String value) {
    return sharedPreferences.setString(key, value);
  }
}

class IntPref extends SharedPref<int> {
  IntPref(SharedPreferences sharedPreferences, String key)
      : super(sharedPreferences, key);

  @override
  int? get() {
    return sharedPreferences.getInt(key);
  }

  @override
  Future _onSetValue(int value) {
    return sharedPreferences.setInt(key, value);
  }
}

class BoolPref extends SharedPref<bool> {
  BoolPref(SharedPreferences sharedPreferences, String key)
      : super(sharedPreferences, key);

  @override
  bool? get() {
    return sharedPreferences.getBool(key);
  }

  @override
  Future _onSetValue(bool value) {
    return sharedPreferences.setBool(key, value);
  }
}

class JsonPref<T> extends SharedPref<Map<String, dynamic>> {
  JsonPref(SharedPreferences sharedPreferences, String key)
      : super(sharedPreferences, key);

  @override
  Map<String, dynamic>? get() {
    final str = sharedPreferences.getString(key);
    if (str != null) {
      final decode = json.decode(str);
      return decode as Map<String, dynamic>;
    }
    return null;
  }

  @override
  Future _onSetValue(Map<String, dynamic> value) {
    final str = json.encode(value);
    return sharedPreferences.setString(key, str);
  }
}

class ModelPref<M> extends SharedPref<M> {
  final ModelMapper<M> _mapper;

  ModelPref(
    SharedPreferences sharedPreferences,
    String key,
    this._mapper,
  ) : super(sharedPreferences, key);

  @override
  M? get() {
    final str = sharedPreferences.getString(key);
    if (str != null) {
      final decode = json.decode(str);
      return _mapper.toModel(decode);
    }
    return null;
  }

  @override
  Future _onSetValue(M value) {
    final map = _mapper.toJson(value);
    final str = json.encode(map);
    return sharedPreferences.setString(key, str);
  }
}
