import 'package:domain/export.dart';
import 'package:domain_impl/src/api/storage/pref.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthPrefImpl extends AuthPref {
  final Pref<String> accessToken;
  final Pref<String> refreshToken;

  AuthPrefImpl(SharedPreferences sharedPreferences)
      : accessToken = StringPref(sharedPreferences, "authToken"),
        refreshToken = StringPref(sharedPreferences, "refreshToken");

  @override
  Future<void> clear() async {
    await accessToken.set(null);
    await refreshToken.set(null);
  }
}
