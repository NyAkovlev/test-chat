import 'package:domain/export.dart';
import 'package:domain_impl/src/api/storage/pref.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserPrefImpl extends UserPref {
  final Pref<UserModel> user;

  UserPrefImpl(
      SharedPreferences sharedPreferences, ModelMapper<UserModel> _mapper)
      : user = ModelPref(sharedPreferences, "user", _mapper);

  @override
  Future<void> clear() async {
    await user.set(null);
  }
}
