import 'package:domain/export.dart';
import 'package:domain_impl/src/api/storage/pref.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DevicePrefImpl extends DevicePref {
  final Pref<int> deviceId;
  final Pref<String> deviceName;
  final Pref<String> deviceSecret;

  DevicePrefImpl(SharedPreferences sharedPreferences)
      : deviceId = IntPref(sharedPreferences, "deviceId"),
        deviceName = StringPref(sharedPreferences, "deviceName"),
        deviceSecret = StringPref(sharedPreferences, "deviceSecret");

  @override
  Future clear() async {
    await deviceId.set(null);
    await deviceName.set(null);
    await deviceSecret.set(null);
  }
}
