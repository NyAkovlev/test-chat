import 'package:stomp_web_socket/stomp_web_socket.dart';

class WebSocketClient extends StompClient {
  WebSocketClient({
    required String url,
    required String token,
    required Function(StompClient?, StompFrame) onConnect,
    required Function(dynamic) onWebSocketError,
    required Function(StompFrame) onDisconnect,
    required Function(StompFrame) onStompError,
    required Function() onWebSocketDone,
  }) : super(
          config: StompConfig.SockJS(
            url: url,
            webSocketConnectHeaders: {
              "Authorization": "Bearer $token",
            },
            stompConnectHeaders: {
              "Authorization": "Bearer $token",
            },
            onConnect: onConnect,
            onDisconnect: onDisconnect,
            onStompError: onStompError,
            onWebSocketError: onWebSocketError,
            onWebSocketDone: onWebSocketDone,
          ),
        );
}
