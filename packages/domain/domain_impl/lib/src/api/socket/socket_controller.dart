import 'dart:async';
import 'dart:convert';

import 'package:domain/export.dart';
import 'package:domain_impl/export.dart';
import 'package:rxdart/rxdart.dart';

import 'client/web_socket_client.dart';

class SocketControllerImpl extends SocketController {
  final BehaviorSubject<SocketState> stateStreamController;
  final ModelMapper<MessageContentModel> messageContentMapper;
  final ModelMapper<MessageModel> messageMapper;
  final WebSocketClient client;
  final String userId;

  SocketControllerImpl(
    this.stateStreamController,
    this.client,
    this.userId,
    this.messageContentMapper,
    this.messageMapper,
  );

  Future<MessageSocketController> connectToChat(int id) async {
    if (!client.connected) {
      await stateStreamController.stream
          .firstWhere((element) => element == SocketState.Connected);
    }
    // ignore: close_sinks
    StreamController<SocketMessageState>? chatController;
    Function? schemeUnsubscribe;
    chatController =
        StreamController<SocketMessageState>.broadcast(onListen: () {
      schemeUnsubscribe = client.subscribe(
        destination: "/sub/chat/$id",
        callback: (frame) {
          if (frame.body == null) {
            return;
          }
          final map = jsonDecode(frame.body!);
          final state =
              SocketMessageState(map["name"], messageMapper.toModel(map["model"]));
          chatController?.add(state);
        },
      );
    }, onCancel: () {
      if (client.connected) {
        client.send(destination: "/send/disconnect");
        schemeUnsubscribe?.call();
      }
    });

    return MessageSocketControllerImpl(
      chatController,
      client,
      id,
      messageContentMapper,
    );
  }

  Stream<SocketState> get stateStream {
    return stateStreamController.stream;
  }

  @override
  SocketState? get state => stateStreamController.value;

  @override
  void close() {
    stateStreamController.close();
  }
}
