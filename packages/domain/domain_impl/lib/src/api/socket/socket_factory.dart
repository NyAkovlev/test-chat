import 'dart:async';

import 'package:domain/export.dart';
import 'package:rxdart/rxdart.dart';

import 'client/export.dart';
import 'socket_controller.dart';

class SocketFactory {
  SocketFactory._();

  static SocketController createController(
    String url,
    AuthPref authPref,
    SecurityGenerator securityGenerator,
    ModelMapper<MessageContentModel> messageContentMapper,
    ModelMapper<MessageModel> messageMapper,
  ) {
    // ignore: close_sinks
    BehaviorSubject<SocketState>? stateController;
    final client = WebSocketClient(
      url: url,
      token: authPref.accessToken.get()!,
      onDisconnect: (frame) {
        stateController?.add(SocketState.Done);
      },
      onStompError: (frame) {
        stateController?.add(SocketState.Error);
      },
      onWebSocketError: (error) {
        stateController?.add(SocketState.Error);
      },
      onConnect: (client, frame) {
        stateController?.add(SocketState.Connected);
      },
      onWebSocketDone: () {
        stateController?.add(SocketState.Done);
      },
    );
    stateController = BehaviorSubject<SocketState>();

    client.activate();
    stateController.done.then((_) {
      client.deactivate();
      stateController = null;
    });
    final separatedUrl = client.config.url.split("/");
    separatedUrl.removeLast();
    final id = separatedUrl.removeLast();
    return SocketControllerImpl(
      stateController!,
      client,
      id,
      messageContentMapper,
      messageMapper,
    );
  }
}
