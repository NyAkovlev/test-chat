import 'dart:async';
import 'dart:convert';

import 'package:domain/export.dart';
import 'package:domain_impl/export.dart';

import 'client/web_socket_client.dart';

class MessageSocketControllerImpl extends MessageSocketController {
  final StreamController<SocketMessageState> schemeStreamController;
  final WebSocketClient client;
  final int id;
  final ModelMapper<MessageContentModel> mapper;

  MessageSocketControllerImpl(
    this.schemeStreamController,
    this.client,
    this.id,
    this.mapper,
  );

  Stream<SocketMessageState> messageStream() {
    return schemeStreamController.stream;
  }

  void sendMessage(int chatId, MessageContentModel message) {
    _send(
      "/send/sendMessage",
      {
        "chatId": chatId,
        "content": mapper.toJson(message),
      },
    );
  }

  void _send(String path, Map<String, dynamic> json) {
    return client.send(
      destination: path,
      body: jsonEncode(json),
    );
  }

  @override
  void close() {
    schemeStreamController.close();
  }
}
