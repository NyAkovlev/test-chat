import 'package:domain/export.dart';
import 'package:domain_impl/src/api/database/database.dart';
import 'package:moor/moor.dart';

/// [UserModel]
@DataClassName('UserDBModel')
class UserTable extends Table {
  String get tableName => "users";

  IntColumn get id => integer()();

  TextColumn get username => text()();

  TextColumn get publicUsername => text()();

  TextColumn get avatar => text().nullable()();

  @override
  Set<Column> get primaryKey => {id};

  static UserDBModel toDbModel(UserModel model) {
    return UserDBModel(
      id: model.id,
      username: model.username,
      publicUsername: model.publicUsername,
      avatar: model.avatar,
    );
  }

  static UserModel toModel(UserDBModel model) {
    return UserModel(
      model.id,
      model.username,
      model.publicUsername,
      model.avatar,
    );
  }
}
