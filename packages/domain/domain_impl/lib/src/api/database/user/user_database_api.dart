import 'package:domain/export.dart';
import 'package:domain_impl/src/api/database/database.dart';
import 'package:domain_impl/src/api/database/user/user_table.dart';
import 'package:moor/moor.dart';

class UserDatabaseApiImpl implements UserDatabaseApi {
  final Database database;

  UserDatabaseApiImpl(this.database);

  Future<UserModel?> getById(int id) {
    final statement = database.select(database.userTable)
      ..where((w) => w.id.equals(id));
    return statement.map(UserTable.toModel).getSingleOrNull();
  }

  Future<void> save(UserModel userModel) async {
    await database.into(database.userTable).insert(
          UserTable.toDbModel(userModel),
          mode: InsertMode.insertOrReplace,
        );
  }
}
