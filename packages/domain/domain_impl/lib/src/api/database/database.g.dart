// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class UserDBModel extends DataClass implements Insertable<UserDBModel> {
  final int id;
  final String username;
  final String publicUsername;
  final String? avatar;
  UserDBModel(
      {required this.id,
      required this.username,
      required this.publicUsername,
      this.avatar});
  factory UserDBModel.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return UserDBModel(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      username: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}username'])!,
      publicUsername: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}public_username'])!,
      avatar:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}avatar']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['username'] = Variable<String>(username);
    map['public_username'] = Variable<String>(publicUsername);
    if (!nullToAbsent || avatar != null) {
      map['avatar'] = Variable<String?>(avatar);
    }
    return map;
  }

  UserTableCompanion toCompanion(bool nullToAbsent) {
    return UserTableCompanion(
      id: Value(id),
      username: Value(username),
      publicUsername: Value(publicUsername),
      avatar:
          avatar == null && nullToAbsent ? const Value.absent() : Value(avatar),
    );
  }

  factory UserDBModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return UserDBModel(
      id: serializer.fromJson<int>(json['id']),
      username: serializer.fromJson<String>(json['username']),
      publicUsername: serializer.fromJson<String>(json['publicUsername']),
      avatar: serializer.fromJson<String?>(json['avatar']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'username': serializer.toJson<String>(username),
      'publicUsername': serializer.toJson<String>(publicUsername),
      'avatar': serializer.toJson<String?>(avatar),
    };
  }

  UserDBModel copyWith(
          {int? id,
          String? username,
          String? publicUsername,
          String? avatar}) =>
      UserDBModel(
        id: id ?? this.id,
        username: username ?? this.username,
        publicUsername: publicUsername ?? this.publicUsername,
        avatar: avatar ?? this.avatar,
      );
  @override
  String toString() {
    return (StringBuffer('UserDBModel(')
          ..write('id: $id, ')
          ..write('username: $username, ')
          ..write('publicUsername: $publicUsername, ')
          ..write('avatar: $avatar')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          username.hashCode, $mrjc(publicUsername.hashCode, avatar.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is UserDBModel &&
          other.id == this.id &&
          other.username == this.username &&
          other.publicUsername == this.publicUsername &&
          other.avatar == this.avatar);
}

class UserTableCompanion extends UpdateCompanion<UserDBModel> {
  final Value<int> id;
  final Value<String> username;
  final Value<String> publicUsername;
  final Value<String?> avatar;
  const UserTableCompanion({
    this.id = const Value.absent(),
    this.username = const Value.absent(),
    this.publicUsername = const Value.absent(),
    this.avatar = const Value.absent(),
  });
  UserTableCompanion.insert({
    this.id = const Value.absent(),
    required String username,
    required String publicUsername,
    this.avatar = const Value.absent(),
  })  : username = Value(username),
        publicUsername = Value(publicUsername);
  static Insertable<UserDBModel> custom({
    Expression<int>? id,
    Expression<String>? username,
    Expression<String>? publicUsername,
    Expression<String?>? avatar,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (username != null) 'username': username,
      if (publicUsername != null) 'public_username': publicUsername,
      if (avatar != null) 'avatar': avatar,
    });
  }

  UserTableCompanion copyWith(
      {Value<int>? id,
      Value<String>? username,
      Value<String>? publicUsername,
      Value<String?>? avatar}) {
    return UserTableCompanion(
      id: id ?? this.id,
      username: username ?? this.username,
      publicUsername: publicUsername ?? this.publicUsername,
      avatar: avatar ?? this.avatar,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (username.present) {
      map['username'] = Variable<String>(username.value);
    }
    if (publicUsername.present) {
      map['public_username'] = Variable<String>(publicUsername.value);
    }
    if (avatar.present) {
      map['avatar'] = Variable<String?>(avatar.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('UserTableCompanion(')
          ..write('id: $id, ')
          ..write('username: $username, ')
          ..write('publicUsername: $publicUsername, ')
          ..write('avatar: $avatar')
          ..write(')'))
        .toString();
  }
}

class $UserTableTable extends UserTable
    with TableInfo<$UserTableTable, UserDBModel> {
  final GeneratedDatabase _db;
  final String? _alias;
  $UserTableTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedIntColumn id = _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn(
      'id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _usernameMeta = const VerificationMeta('username');
  @override
  late final GeneratedTextColumn username = _constructUsername();
  GeneratedTextColumn _constructUsername() {
    return GeneratedTextColumn(
      'username',
      $tableName,
      false,
    );
  }

  final VerificationMeta _publicUsernameMeta =
      const VerificationMeta('publicUsername');
  @override
  late final GeneratedTextColumn publicUsername = _constructPublicUsername();
  GeneratedTextColumn _constructPublicUsername() {
    return GeneratedTextColumn(
      'public_username',
      $tableName,
      false,
    );
  }

  final VerificationMeta _avatarMeta = const VerificationMeta('avatar');
  @override
  late final GeneratedTextColumn avatar = _constructAvatar();
  GeneratedTextColumn _constructAvatar() {
    return GeneratedTextColumn(
      'avatar',
      $tableName,
      true,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, username, publicUsername, avatar];
  @override
  $UserTableTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'users';
  @override
  final String actualTableName = 'users';
  @override
  VerificationContext validateIntegrity(Insertable<UserDBModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('username')) {
      context.handle(_usernameMeta,
          username.isAcceptableOrUnknown(data['username']!, _usernameMeta));
    } else if (isInserting) {
      context.missing(_usernameMeta);
    }
    if (data.containsKey('public_username')) {
      context.handle(
          _publicUsernameMeta,
          publicUsername.isAcceptableOrUnknown(
              data['public_username']!, _publicUsernameMeta));
    } else if (isInserting) {
      context.missing(_publicUsernameMeta);
    }
    if (data.containsKey('avatar')) {
      context.handle(_avatarMeta,
          avatar.isAcceptableOrUnknown(data['avatar']!, _avatarMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  UserDBModel map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return UserDBModel.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $UserTableTable createAlias(String alias) {
    return $UserTableTable(_db, alias);
  }
}

abstract class _$Database extends GeneratedDatabase {
  _$Database(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  late final $UserTableTable userTable = $UserTableTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [userTable];
}
