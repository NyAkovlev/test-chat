import 'dart:io';

import 'package:domain_impl/src/api/database/user/user_table.dart';
import 'package:moor/ffi.dart';
import 'package:moor/moor.dart';
import 'package:path_provider/path_provider.dart';

part 'database.g.dart';

@UseMoor(
  tables: [UserTable],
)
class Database extends _$Database {
  Database() : super(_openConnection());

  @override
  int get schemaVersion => 1;

  static LazyDatabase _openConnection() {
    return LazyDatabase(() async {
      final dbFolder = await getApplicationDocumentsDirectory();
      final file = File(dbFolder.path + Platform.pathSeparator + 'db.sqlite');
      return VmDatabase(file);
    });
  }
}
