import 'package:domain/export.dart';
import 'package:uuid/uuid.dart';

class SecurityGeneratorImpl extends SecurityGenerator {
  final uuid = Uuid();

  String uuidV4() {
    return uuid.v4();
  }
}
