class RestClientError {
  final String message;
  final RestClientErrorCase errorCase;

  RestClientError(this.errorCase, [this.message = ""]);
}

enum RestClientErrorCase {
  BigUploadFile,
  Undefined,
}
