import 'dart:typed_data';

import 'package:domain/export.dart';
import 'package:domain_impl/src/api/network/client/rest_client.dart';

class FileNetworkApiImpl extends FileNetworkApi {
  final RestClient restClient;
  final SecurityGenerator securityGenerator;
  final AuthPref authPref;

  FileNetworkApiImpl(this.restClient, this.securityGenerator, this.authPref);

  Future uploadFile(String path, String uuid) async {
    await restClient.multipart(
        headers: {"Authorization": "Bearer ${authPref.accessToken.get()}"},
        path: "/files/upload",
        fileField: "file",
        filePath: path,
        fields: {
          "name": uuid,
          "uuid": uuid,
        });
  }

  Future<Uint8List> downloadFile(String uuid) async {
    final response = await restClient.get(
      "/files/$uuid",
      headers: {"Authorization": "Bearer ${authPref.accessToken.get()}"},
    );
    return response.bodyBytes;
  }
}
