import 'package:domain/export.dart';
import 'package:graphql_client/graphql_client.dart';
import 'package:graphql_schema/export.dart';

class UserApiImpl extends UserApi {
  final GraphQlClient _client;
  final ModelMapper<UserModel> _mapper;

  UserApiImpl(this._client, this._mapper);

  Future<UserModel> getUser() async {
    final response = await _client.request(UserQuery());
    return _mapper.toModel(response["user"]);
  }

  Future<ListResponse<UserModel>> findUser(
    String username,
    int size,
    int page,
  ) async {
    final response = await _client.request(FindUserQuery(username, size, page));
    final map = response["findUser"];
    return ListResponse(
      (map["items"] as List).map((v) => _mapper.toModel(v)),
      map["totalPages"],
    );
  }

  Future<UserModel> changeAvatar(String fileId) async {
    final response = await _client.request(ChangeAvatarOfUserMutation(fileId));
    final map = response["changeAvatarOfUser"];
    return _mapper.toModel(map);
  }
}
