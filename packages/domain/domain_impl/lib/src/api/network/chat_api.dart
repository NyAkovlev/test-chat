import 'package:domain/export.dart';
import 'package:graphql_client/graphql_client.dart';
import 'package:graphql_schema/export.dart';

class ChatApiImpl extends ChatApi {
  final GraphQlClient _client;
  final ModelMapper<ChatUserRole> _chatUserRoleMapper;
  final ModelMapper<ChatModel> _chatMapper;
  final ModelMapper<ChatUserModel> _chatUserModelMapper;

  ChatApiImpl(this._client, this._chatUserRoleMapper, this._chatMapper,
      this._chatUserModelMapper);

  Future<ListResponse<ChatModel>> chats(
    int size,
    int page,
  ) async {
    final response = await _client.request(MyChatsQuery(size, page));
    return ListResponse(
      (response["myChats"]["items"] as List)
          .map((e) => _chatMapper.toModel(e))
          .toList(),
      response["myChats"]["totalPages"],
    );
  }

  Future<ListResponse<ChatUserModel>> memberOfChat(
    int size,
    int page,
    int chatId,
  ) async {
    final response = await _client.request(MembersOfChatQuery(
      size,
      page,
      chatId,
    ));
    return ListResponse(
      (response["membersOfChat"]["items"] as List)
          .map((e) => _chatUserModelMapper.toModel(e))
          .toList(),
      response["membersOfChat"]["totalPages"],
    );
  }

  Future<ChatModel?> personalChat(int secondUserId) async {
    final response = await _client.request(PersonalChatQuery(
      secondUserId,
    ));
    final map = response["personalChat"];
    if (map == null) {
      return null;
    }
    return _chatMapper.toModel(response["personalChat"]);
  }

  Future<ChatModel> createPersonalChat(int secondUserId) async {
    final response = await _client.request(CreatePersonalChatMutation(
      secondUserId,
    ));
    return _chatMapper.toModel(response["createPersonalChat"]);
  }

  Future<ChatModel> createChat() async {
    final response = await _client.request(CreateChatMutation());
    return _chatMapper.toModel(response["createChat"]);
  }

  Future deleteChat(int chatId) async {
    await _client.request(DeleteChatMutation(
      chatId,
    ));
  }

  Future<List<ChatUserRole>> chatRole() async {
    final response = await _client.request(ChatRoleQuery());
    return (response["chatRole"] as List)
        .map((e) => _chatUserRoleMapper.toModel(e))
        .toList();
  }

  Future<ChatModel> changeAvatar(int chatId, String fileId) async {
    final response =
        await _client.request(ChangeAvatarOfChatMutation(chatId, fileId));
    return _chatMapper.toModel(response["changeAvatarOfChat"]);
  }
}
