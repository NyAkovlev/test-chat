import 'package:domain/export.dart';

class ImageUrlApiImpl extends ImageUrlApi {
  final String url;

  ImageUrlApiImpl(this.url);

  String getUrl(String id) {
    return url + "/" + id;
  }
}
