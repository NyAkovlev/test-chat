import 'package:domain/export.dart';

class TokenInvalidateListenerImpl extends TokenInvalidateListener {
  final _listeners = <Function()>{};

  @override
  void addListener(Function() listener) {
    _listeners.add(listener);
  }

  @override
  void removeListener(Function() listener) {
    _listeners.remove(listener);
  }

  void notifyListeners() {
    for (var item in _listeners) {
      try {
        item.call();
      } catch (e, s) {
        print(s);
      }
    }
  }
}
