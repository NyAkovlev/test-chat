import 'package:domain/export.dart';
import 'package:domain_impl/src/api/network/client/auth_interceptor.dart';
import 'package:domain_impl/src/api/network/client/refresh_token_interceptor.dart';
import 'package:graphql_client/graphql_client.dart';

class MyGraphQlClient extends GraphQlClient {
  MyGraphQlClient(
    AuthPref authPref,
    String url,
    DevicePref devicePref,
    ModelMapper<AuthResponse> mapper,
    TokenInvalidateListener tokenInvalidateListener,
  ) : super(url) {
    addInterceptor(AuthInterceptor(authPref));
    addInterceptor(
      RefreshTokenInterceptor(
        authPref,
        this,
        devicePref,
        mapper,
        tokenInvalidateListener,
      ),
    );
  }
}
