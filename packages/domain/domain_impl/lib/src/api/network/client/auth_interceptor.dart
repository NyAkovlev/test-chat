import 'package:domain/export.dart';
import 'package:domain_impl/export.dart';
import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_client/graphql_client.dart';
import 'package:gql_http_link/gql_http_link.dart';
import 'package:graphql_schema/export.dart';

class AuthInterceptor extends GraphQlInterceptor {
  final AuthPref _authPref;

  AuthInterceptor(this._authPref);

  Future<Request?> onRequest(Request request) async {
    if (request is! RefreshTokenMutation) {
      final token = _authPref.accessToken.get();
      if (token != null) {
        var headers = request.context.entry<HttpLinkHeaders>()?.headers;
        return request.withContextEntry(
          HttpLinkHeaders(
            headers: {
              if (headers != null) ...headers,
              "Authorization": "Bearer $token",
            },
          ),
        );
      }
    }
  }
}
