import 'dart:io';

import 'package:domain_impl/src/api/network/error/rest_client_error.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';

class RestClient {
  final String url;

  RestClient(this.url);

  Future<http.StreamedResponse> multipart({
    required String path,
    required String fileField,
    required String filePath,
    String? filename,
    Map<String, String>? fields,
    MediaType? contentType,
    final Map<String, String>? headers,
  }) async {
    try {
      var request = new http.MultipartRequest("POST", Uri.parse(url + path));

      fields?.forEach((key, value) {
        request.fields[key] = value;
      });
      if (headers != null) {
        request.headers.addAll(headers);
      }
      request.files.add(await http.MultipartFile.fromPath(
        fileField,
        filePath,
        filename: filename,
        contentType: contentType,
      ));

      final response = await request.send();
      if (response.statusCode < 200 || response.statusCode >= 300) {
        throw RestClientError(RestClientErrorCase.Undefined);
      }
      return response;
    } catch (e) {
      if (e is RestClientError) {
        rethrow;
      }
      if (e is SocketException && e.osError!.errorCode == 32) {
        throw RestClientError(RestClientErrorCase.BigUploadFile);
      }
      throw RestClientError(RestClientErrorCase.Undefined, e.toString());
    }
  }

  Future<http.Response> get(String path, {Map<String, String>? headers}) async {
    try {
      final response = await http.get(
        Uri.parse(url + path),
        headers: headers,
      );
      if (response.statusCode < 200 || response.statusCode >= 300) {
        throw RestClientError(RestClientErrorCase.Undefined);
      }
      return response;
    } catch (e) {
      if (e is RestClientError) {
        rethrow;
      }
      if (e is SocketException && e.osError!.errorCode == 32) {
        throw RestClientError(RestClientErrorCase.BigUploadFile);
      }
      throw RestClientError(RestClientErrorCase.Undefined, e.toString());
    }
  }
}
