import 'package:domain/export.dart';
import 'package:domain_impl/export.dart';
import 'package:domain_impl/src/api/network/client/token_invalidate_listener.dart';
import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_client/graphql_client.dart';
import 'package:gql_http_link/gql_http_link.dart';
import 'package:graphql_schema/export.dart';

class RefreshTokenInterceptor extends GraphQlInterceptor {
  final GraphQlClient _graphQlClient;
  final AuthPref _authPref;
  final DevicePref _devicePref;
  final ModelMapper<AuthResponse> _mapper;
  final TokenInvalidateListener _tokenInvalidateListener;

  RefreshTokenInterceptor(
    this._authPref,
    this._graphQlClient,
    this._devicePref,
    this._mapper,
    this._tokenInvalidateListener,
  );

  @override
  Future<Response?> onError(Request request, e) async {
    if (e is HttpLinkParserException && e.response.statusCode == 401) {
      final deviceSecret = _devicePref.deviceSecret.get();
      final deviceId = _devicePref.deviceId.get();
      final refreshToken = _authPref.refreshToken.get();
      if (deviceSecret != null && deviceId != null && refreshToken != null) {
        try {
          final response = await _graphQlClient.request(
            RefreshTokenMutation(
              deviceSecret,
              deviceId,
              refreshToken,
            ),
          );
          final tokens = _mapper.toModel(response["refreshToken"]);
          _authPref.accessToken.set(tokens.accessToken);
          _authPref.refreshToken.set(tokens.refreshToken);
          return _graphQlClient.requestWithResponse(request);
        } catch (e) {
          if (e is GraphQLResponseError) {
            if (e.errorCase == GraphQlErrorCase.InvalidDevice) {
              await _devicePref.clear();
            }
            (_tokenInvalidateListener as TokenInvalidateListenerImpl)
                .notifyListeners();
          }
          rethrow;
        }
      }
    }
    return super.onError(request, e);
  }
}
