import 'package:graphql_client/graphql_client.dart';
import 'package:graphql_schema/export.dart';
import 'package:domain/export.dart';

class DeviceApiImpl extends DeviceApi {
  final GraphQlClient _client;
  final ModelMapper<DeviceModel> _mapper;

  DeviceApiImpl(this._client, this._mapper);

  Future<DeviceModel> createDevice(String name, String secret) async {
    final response = await _client.request(CreateDeviceMutation(name, secret));
    return _mapper.toModel(response["createDevice"]);
  }
}
