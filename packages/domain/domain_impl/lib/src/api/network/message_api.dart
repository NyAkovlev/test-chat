import 'package:domain/export.dart';
import 'package:graphql_client/graphql_client.dart';
import 'package:graphql_schema/export.dart';
import 'package:intl/intl.dart';

class MessageApiImpl extends MessageApi {
  final GraphQlClient _client;
  final ModelMapper<MessageModel> _messageMapper;
  final ModelMapper<MessageContentModel> _messageContentMapper;
  final DateFormat dateFormat;

  MessageApiImpl(
    this._client,
    this._messageMapper,
    this.dateFormat,
    this._messageContentMapper,
  );

  Future<ListResponse<MessageModel>> messagesOfChatBefore(
    int chatId,
    DateTime before,
      int size,
  ) async {
    final response = await _client
        .request(MessagesOfChatBeforeQuery(size, chatId, before, dateFormat));
    final map = response["messagesOfChatBefore"] as Map<String, dynamic>;
    return ListResponse(
        (map["items"] as List).map((e) => _messageMapper.toModel(e)),
        map["totalPages"]);
  }

  Future<MessageModel> sendMessage(
    int chatId,
    MessageContentModel messageContentModel,
  ) async {
    final response = await _client.request(SendMessageMutation(
      chatId,
      _messageContentMapper.toJson(messageContentModel),
    ));
    final map = response["sendMessage"] as Map<String, dynamic>;
    return _messageMapper.toModel(map);
  }
}
