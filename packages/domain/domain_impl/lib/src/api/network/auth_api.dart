import 'package:domain/export.dart';
import 'package:graphql_client/graphql_client.dart';
import 'package:graphql_schema/export.dart';
import 'package:domain/export.dart';

class AuthApiImpl extends AuthApi {
  final GraphQlClient _client;
  final ModelMapper<AuthResponse> _mapper;

  AuthApiImpl(this._client, this._mapper);

  Future<AuthResponse> signIn(
    String deviceSecret,
    int deviceId,
    String username,
    String password,
  ) async {
    final response = await _client.request(SignInMutation(
      deviceSecret,
      deviceId,
      username,
      password,
    ));
    return _mapper.toModel(response["signIn"]);
  }

  Future<AuthResponse> signUp(
    String deviceSecret,
    int deviceId,
    String username,
    String password,
  ) async {
    final response = await _client.request(SignUpMutation(
      deviceSecret,
      deviceId,
      username,
      password,
    ));
    return _mapper.toModel(response["signUp"]);
  }

  Future<AuthResponse> refreshToken(
    String deviceSecret,
    int deviceId,
    String refreshToken,
  ) async {
    final response = await _client.request(RefreshTokenMutation(
      deviceSecret,
      deviceId,
      refreshToken,
    ));
    return _mapper.toModel(response["refreshToken"]);
  }
}
