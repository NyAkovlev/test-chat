import 'package:graphql_client/graphql_client.dart';
import 'package:gql_exec/gql_exec.dart';
import 'api_error.dart';

class ApiErrorMapper {
  ApiError? onError(dynamic e) {
    if (e is GraphQLResponseError) {
      return onError(e.errorCase);
    } else if (e is GraphQlErrorCase) {
      return graphQlErrorCaseToError(e);
    }
    return ApiError(ApiErrorCase.Undefined, null, e);
  }

  ApiError graphQlErrorCaseToError(GraphQlErrorCase errorCase) {
    switch (errorCase) {
      case GraphQlErrorCase.EmptyResponse:
        return ApiError(ApiErrorCase.EmptyResponse);
      case GraphQlErrorCase.InvalidToken:
      case GraphQlErrorCase.InvalidDevice:
      case GraphQlErrorCase.Undefined:
        return ApiError(ApiErrorCase.Undefined);
    }
  }
}
