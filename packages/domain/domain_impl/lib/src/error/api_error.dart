class ApiError {
  final ApiErrorCase errorCase;
  final String? description;
  final dynamic parent;

  ApiError(
    this.errorCase, [
    this.description,
    this.parent,
  ]);
}

enum ApiErrorCase {
  InvalidInputParams,
  EmptyResponse,
  Undefined,
}
