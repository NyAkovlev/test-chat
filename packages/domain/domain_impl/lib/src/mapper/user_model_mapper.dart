import 'package:domain/export.dart';

class UserModelMapper extends ModelMapper<UserModel> {
  @override
  Map<String, dynamic> toJson(UserModel model) {
    return {
      "id": model.id,
      "username": model.username,
      "publicUsername": model.publicUsername,
      "avatar": model.avatar,
    };
  }

  @override
  UserModel toModel(Map<String, dynamic> map) {
    return UserModel(
      map["id"],
      map["username"],
      map["publicUsername"],
      map["avatar"],
    );
  }
}
