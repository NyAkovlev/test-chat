import 'package:domain/export.dart';

class ChatUserMapper extends ModelMapper<ChatUserModel> {
  @override
  Map<String, dynamic> toJson(ChatUserModel model) {
    return {
      "userId": model.userId,
      "chatId": model.chatId,
      "invited": model.invited.toIso8601String(),
      "role": model.role,
    };
  }

  @override
  ChatUserModel toModel(Map<String, dynamic> map) {
    return ChatUserModel(
      map["userId"],
      map["chatId"],
      DateTime.parse(map["invited"]),
      map["role"],
    );
  }
}
