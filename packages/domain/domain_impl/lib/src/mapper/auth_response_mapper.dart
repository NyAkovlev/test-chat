import 'package:domain/export.dart';

class AuthResponseMapper extends ModelMapper<AuthResponse> {
  @override
  Map<String, dynamic> toJson(AuthResponse model) {
    return {
      "accessToken": model.accessToken,
      "refreshToken": model.refreshToken,
    };
  }

  @override
  AuthResponse toModel(Map<String, dynamic> map) {
    return AuthResponse(
      map["accessToken"],
      map["refreshToken"],
    );
  }
}
