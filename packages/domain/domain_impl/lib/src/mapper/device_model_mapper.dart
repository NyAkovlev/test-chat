import 'package:domain/export.dart';

class DeviceModelMapper extends ModelMapper<DeviceModel> {
  @override
  Map<String, dynamic> toJson(DeviceModel model) {
    return {
      "id": model.id,
      "name": model.name,
    };
  }

  @override
  DeviceModel toModel(Map<String, dynamic> map) {
    return DeviceModel(
      map["id"],
      map["name"],
      map["secret"],
    );
  }
}
