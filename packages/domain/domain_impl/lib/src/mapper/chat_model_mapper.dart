import 'package:domain/export.dart';

class ChatModelMapper extends ModelMapper<ChatModel> {
  @override
  Map<String, dynamic> toJson(ChatModel model) {
    return {
      "id": model.id,
      "name": model.name,
      "avatar": model.avatar,
      "canInvite": model.canInvite,
      "created": model.created.toIso8601String(),
      "updated": model.updated.toIso8601String(),
    };
  }

  @override
  ChatModel toModel(Map<String, dynamic> map) {
    return ChatModel(
      map["id"],
      map["name"],
      map["avatar"],
      map["canInvite"],
      DateTime.parse(map["created"]),
      DateTime.parse(map["updated"]),
    );
  }
}
