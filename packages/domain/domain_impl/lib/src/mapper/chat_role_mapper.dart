import 'package:domain/export.dart';

class ChatUserRoleMapper extends ModelMapper<ChatUserRole> {
  @override
  Map<String, dynamic> toJson(ChatUserRole model) {
    return {
      "name": model.name,
      "permission": model.permission
          .map((e) => ChatUserPermissionHelper.instance.modelToString(e))
          .toList(),
    };
  }

  @override
  ChatUserRole toModel(Map<String, dynamic> map) {
    return ChatUserRole(
      map["name"],
      (map["permission"] as List)
          .map((e) => ChatUserPermissionHelper.instance.modelFromString(e))
          .toSet(),
    );
  }
}
