import 'package:domain/export.dart';

class MessageContentItemMapper extends ModelMapper<MessageContentItemModel> {
  @override
  Map<String, dynamic> toJson(MessageContentItemModel model) {
    return {
      "name": model.name,
      "value": model.value,
    };
  }

  @override
  MessageContentItemModel toModel(Map<String, dynamic> map) {
    return MessageContentItemModel(
      map["name"],
      map["value"],
    );
  }
}
