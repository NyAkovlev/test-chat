import 'package:domain/export.dart';

class MessageMapper extends ModelMapper<MessageModel> {
  final ModelMapper<MessageContentModel> _mapper;

  MessageMapper(this._mapper);

  @override
  Map<String, dynamic> toJson(MessageModel model) {
    return {
      "id": model.id,
      "chatId": model.chatId,
      "created": model.created,
      "content": _mapper.toJson(model.content),
      "sender": model.sender,
    };
  }

  @override
  MessageModel toModel(Map<String, dynamic> map) {
    return MessageModel(
      map["id"],
      map["chatId"],
      DateTime.parse(map["created"]),
      _mapper.toModel(map["content"]),
      map["sender"],
    );
  }
}
