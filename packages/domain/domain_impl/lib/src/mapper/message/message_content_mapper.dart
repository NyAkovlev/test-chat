import 'package:domain/export.dart';

class MessageContentMapper extends ModelMapper<MessageContentModel> {
  final ModelMapper<MessageContentItemModel> _mapper;

  MessageContentMapper(this._mapper);

  @override
  Map<String, dynamic> toJson(MessageContentModel model) {
    return {
      "items": model.items.map((e) => _mapper.toJson(e)).toList(),
    };
  }

  @override
  MessageContentModel toModel(Map<String, dynamic> map) {
    return MessageContentModel(
      (map["items"] as List).map((e) => _mapper.toModel(e)).toList(),
    );
  }
}
