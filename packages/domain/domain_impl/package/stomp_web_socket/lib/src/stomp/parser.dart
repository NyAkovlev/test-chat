import 'stomp_frame.dart';

abstract class Parser {
  bool get escapeHeaders;

  set escapeHeaders(bool value);

  void parseData(dynamic data);

  dynamic serializeFrame(StompFrame frame);
}
