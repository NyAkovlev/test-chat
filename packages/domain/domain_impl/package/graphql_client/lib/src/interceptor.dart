import 'package:gql_exec/src/request.dart';
import 'package:gql_exec/src/response.dart';

class GraphQlInterceptor {
  Future<Request?> onRequest(Request request) async => null;

  Future<Response?> onResponse(Request request, Response response) async =>
      null;

  Future<Response?> onError(Request request, e) async => null;
}
