import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_client/graphql_client.dart';

class GraphQLResponseError {
  final List<GraphQLError>? errors;
  final Context? context;
  final GraphQlErrorCase errorCase;

  GraphQLResponseError(
    List<GraphQLError> errors,
    this.context,
  )   : errors = errors,
        errorCase = _getCase(errors);

  GraphQLResponseError.errorCase(
    this.errorCase,
    this.context,
  ) : errors = null;

  static _getCase(List<GraphQLError> errors) {
    if (errors.isEmpty) {
      return GraphQlErrorCase.Undefined;
    }
    final error = errors.first;
    var splintedMessage = error.message.split(":");
    if (splintedMessage.length == 3) {
      final message = splintedMessage[2];
      return _messageToCase(message);
    } else {
      return GraphQlErrorCase.Undefined;
    }
  }

  static GraphQlErrorCase _messageToCase(String message) {
    if (message ==
        "The token is expired or invalid The JWT token is expired or invalid") {
      return GraphQlErrorCase.InvalidToken;
    } else if (message == "Model not found a model Device not found") {
      return GraphQlErrorCase.InvalidDevice;
    }
    return GraphQlErrorCase.Undefined;
  }
}
