enum GraphQlErrorCase {
  InvalidToken,
  EmptyResponse,
  Undefined,
  InvalidDevice,
}
