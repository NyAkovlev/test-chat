import 'package:gql_exec/gql_exec.dart';
import 'package:gql_http_link/gql_http_link.dart';

import 'graph_ql_error_case.dart';
import 'graph_ql_response_error.dart';
import 'interceptor.dart';

class GraphQlClient {
  final List<GraphQlInterceptor> _interceptors = [];
  final HttpLink _link;
  final dynamic? Function(dynamic)? mapError;

  GraphQlClient(
    String url, {
    this.mapError,
  }) : _link = HttpLink(url);

  Future<Map<String, dynamic>> request(Request request) async {
    return requestWithResponse(request).then((v) {
      return v.data!;
    });
  }

  Future<Response> requestWithResponse(Request request) async {
    var requestHolder = request;
    requestHolder = await _onRequest(requestHolder);
    try {
      try {
        var responseHolder = await _link.request(requestHolder).first;
        responseHolder = await _onResponse(requestHolder, responseHolder);
        final errors = responseHolder.errors;
        final data = responseHolder.data;
        if (errors != null) {
          throw GraphQLResponseError(
            errors,
            responseHolder.context,
          );
        }
        if (data != null) {
          return responseHolder;
        } else {
          throw GraphQLResponseError.errorCase(
            GraphQlErrorCase.EmptyResponse,
            responseHolder.context,
          );
        }
      } catch (e, s) {
        final response = await _onError(requestHolder, e);
        final errors = response.errors;
        final data = response.data;
        if (errors != null) {
          throw GraphQLResponseError(errors, response.context);
        }
        if (data != null) {
          return response;
        } else {
          throw GraphQLResponseError.errorCase(
            GraphQlErrorCase.EmptyResponse,
            response.context,
          );
        }
      }
    } catch (e) {
      final result = mapError?.call(e);
      if (result != null) throw result;
      rethrow;
    }
  }

  void addInterceptor(GraphQlInterceptor interceptor) {
    _interceptors.add(interceptor);
  }

  void removeInterceptor(GraphQlInterceptor interceptor) {
    _interceptors.remove(interceptor);
  }

  Future<Request> _onRequest(Request request) async {
    var mutableRequest = request;
    for (var item in _interceptors) {
      final result = await item.onRequest(request);
      if (result != null) {
        mutableRequest = result;
      }
    }
    return mutableRequest;
  }

  Future<Response> _onResponse(Request request, Response response) async {
    var responseHolder = response;
    for (var item in _interceptors) {
      final result = await item.onResponse(request, responseHolder);
      if (result != null) {
        responseHolder = result;
      }
    }
    return responseHolder;
  }

  Future<Response> _onError(Request request, e) async {
    for (var item in _interceptors) {
      final result = await item.onError(request, e);
      if (result != null) {
        return result;
      }
    }
    throw e;
  }

  void close() {
    _link.dispose();
  }
}
