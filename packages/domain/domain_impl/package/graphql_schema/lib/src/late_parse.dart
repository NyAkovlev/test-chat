import 'package:gql/language.dart';
import 'package:gql_exec/gql_exec.dart';

class LateParse {
  final String _source;

  LateParse(this._source);

  Operation? _operation;

  Operation get operation =>
      _operation ??= Operation(document: parseString(_source));
}
