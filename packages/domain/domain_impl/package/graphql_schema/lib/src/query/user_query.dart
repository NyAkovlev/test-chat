
import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_schema/src/late_parse.dart';

class UserQuery extends Request {
  UserQuery()
      : super(
          operation: _document.operation,
        );
}

final _document = LateParse(""" 
query user{
    user{
        id
        publicUsername
        username
        avatar
    }
}
""");
