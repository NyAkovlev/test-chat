import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_schema/src/late_parse.dart';

class FindUserQuery extends Request {
  FindUserQuery(
    String username,
    int size,
    int page,
  ) : super(
          operation: _document.operation,
          variables: {
            "username": username,
            "size": size,
            "page": page,
          },
        );
}

final _document = LateParse(""" 
query findUser(
   \$username: String
   \$size: Int!
   \$page: Int!
){
    findUser(username:\$username,size:\$size,page:\$page){
        items{
            id
            publicUsername
            username
            avatar
        }
        totalPages
    }
}
""");
