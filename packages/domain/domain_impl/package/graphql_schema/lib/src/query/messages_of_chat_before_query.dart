import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_schema/src/late_parse.dart';
import 'package:intl/intl.dart';

class MessagesOfChatBeforeQuery extends Request {
  MessagesOfChatBeforeQuery(
    int size,
    int chatId,
    DateTime before,
    DateFormat dateFormat,
  ) : super(
          operation: _document.operation,
          variables: {
            "size": size,
            "chatId": chatId,
            "before": before.toUtc().toIso8601String(),
          },
        );
}

final _document = LateParse(""" 
query messagesOfChatBefore(
   \$size:Int!
   \$chatId:Long!
   \$before:OffsetDateTime
){
    messagesOfChatBefore(
        size:\$size
        chatId:\$chatId
        before:\$before
    ){
        items{
            chatId
            created
            content{
                items{
                    name
                    value
                }
            }
            id
            sender
        }
        totalPages
    }
}
""");
