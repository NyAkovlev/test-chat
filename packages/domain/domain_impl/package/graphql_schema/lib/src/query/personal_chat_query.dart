import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_schema/src/late_parse.dart';

class PersonalChatQuery extends Request {
  PersonalChatQuery(int secondUserId)
      : super(
          operation: _document.operation,
          variables: {
            "secondUserId":secondUserId
          },
        );
}

final _document = LateParse(""" 
query personalChat(\$secondUserId:Long!){
    personalChat(secondUserId:\$secondUserId){
        id
        avatar
        canInvite
        name
        created
        updated
        deleted
    }
}
""");
