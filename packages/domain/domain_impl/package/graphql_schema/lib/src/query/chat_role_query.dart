import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_schema/src/late_parse.dart';

class ChatRoleQuery extends Request {
  ChatRoleQuery()
      : super(
          operation: _document.operation,
          variables: {},
        );
}

final _document = LateParse(""" 
query chatRole{
    chatRole{
        name
        permission
    }
}
""");
