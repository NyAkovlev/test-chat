import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_schema/src/late_parse.dart';

class MembersOfChatQuery extends Request {
  MembersOfChatQuery(
    int size,
    int page,
    int chatId,
  ) : super(
          operation: _document.operation,
          variables: {
            "chatId": chatId,
            "size": size,
            "page": page,
          },
        );
}

final _document = LateParse(""" 
query membersOfChat(
   \$size: Int!
   \$chatId: Long!
   \$page: Int!
){
    membersOfChat(
        size: \$size
        chatId: \$chatId
        page: \$page
    ){
        items{
            user{
                id
            }
            role
            invited
        }
        totalPages
    }
}
""");
