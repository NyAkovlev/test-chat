import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_schema/src/late_parse.dart';

class MyChatsQuery extends Request {
  MyChatsQuery(
    int size,
    int page,
  ) : super(
          operation: _document.operation,
          variables: {
            "size": size,
            "page": page,
          },
        );
}

final _document = LateParse(""" 
query myChats(\$size: Int!, \$page: Int!){
    myChats(size: \$size, page: \$page){
        items{
            id
            avatar
            canInvite
            name
            created
            updated
            deleted
        }
        totalPages
    }
}
""");
