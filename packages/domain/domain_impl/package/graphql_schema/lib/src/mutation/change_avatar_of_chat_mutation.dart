import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_schema/src/late_parse.dart';

class ChangeAvatarOfChatMutation extends Request {
  ChangeAvatarOfChatMutation(int chatId, String file)
      : super(
          operation: _document.operation,
          variables: {
            "chatId": chatId,
            "file": file,
          },
        );
}

final _document = LateParse(""" 
mutation changeAvatarOfChat(
    \$chatId:Long!
    \$file:String
){
    changeAvatarOfChat(
        chatId: \$chatId
        fileId:\$file
    ){
        id
        avatar
        canInvite
        name
        created
        updated
        deleted
    }
}
""");
