
import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_schema/src/late_parse.dart';

class SignUpMutation extends Request {
  SignUpMutation(
    String deviceSecret,
    int deviceId,
    String username,
    String password,
  ) : super(
          operation: _document.operation,
          variables: {
            "deviceSecret": deviceSecret,
            "deviceId": deviceId,
            "username": username,
            "password": password,
          },
        );
}

final _document = LateParse(""" 
mutation signUp(
    \$deviceSecret:String,
    \$deviceId:Long!,
    \$username:String,
    \$password:String
){
    signUp(
        deviceSecret:\$deviceSecret,
        deviceId:\$deviceId,
        username:\$username,
        password:\$password
    ){
        accessToken
        refreshToken
    }
}
""");
