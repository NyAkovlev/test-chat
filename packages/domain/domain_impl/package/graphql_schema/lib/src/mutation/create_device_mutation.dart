import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_schema/src/late_parse.dart';

class CreateDeviceMutation extends Request {
  CreateDeviceMutation(String name, String secret)
      : super(
          operation: _document.operation,
          variables: {
            "name": name,
            "secret": secret,
          },
        );
}

final _document = LateParse(""" 
mutation createDevice(
    \$name: String
    \$secret: String
){
    createDevice(
        name: \$name,
        secret: \$secret
    ){
        id
        name
    }
}
""");
