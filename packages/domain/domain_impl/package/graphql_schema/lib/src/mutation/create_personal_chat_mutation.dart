import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_schema/src/late_parse.dart';

class CreatePersonalChatMutation extends Request {
  CreatePersonalChatMutation(int secondUserId)
      : super(
          operation: _document.operation,
          variables: {
            "secondUserId": secondUserId,
          },
        );
}

final _document = LateParse(""" 
mutation createPersonalChat(\$secondUserId: Long!){
    createPersonalChat(secondUserId: \$secondUserId){
        id
        avatar
        canInvite
        name
        created
        updated
        deleted
    }
}
""");
