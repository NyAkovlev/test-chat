import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_schema/src/late_parse.dart';

class DeleteChatMutation extends Request {
  DeleteChatMutation(int chatId)
      : super(
          operation: _document.operation,
          variables: {
            "chatId": chatId,
          },
        );
}

final _document = LateParse(""" 
mutation deleteChat(\$chatId: Long!){
    deleteChat(chatId: \$chatId)
}
""");
