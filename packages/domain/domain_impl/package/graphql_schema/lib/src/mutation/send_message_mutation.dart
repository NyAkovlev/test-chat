import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_schema/src/late_parse.dart';

class SendMessageMutation extends Request {
  SendMessageMutation(
    int chatId,
    Map<String, dynamic> content,
  ) : super(
          operation: _document.operation,
          variables: {
            "chatId": chatId,
            "content": content,
          },
        );
}

final _document = LateParse(""" 
mutation sendMessage(
    \$chatId: Long!
    \$content: MessageContentInput
){
    sendMessage(
        chatId:\$chatId
        content:\$content
    ){
        id
        chatId
        content{
            items{
                name
                value
            }
        }
        sender
        created
    }
}
""");
