
import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_schema/src/late_parse.dart';

class RefreshTokenMutation extends Request {
  RefreshTokenMutation(
    String deviceSecret,
    int deviceId,
    String refreshToken,
  ) : super(
          operation: _document.operation,
          variables: {
            "deviceSecret": deviceSecret,
            "deviceId": deviceId,
            "refreshToken": refreshToken,
          },
        );
}

final _document = LateParse(""" 
mutation refreshToken(
    \$deviceSecret: String
    \$deviceId: Long!
    \$refreshToken: String
) {
    refreshToken(
        deviceSecret: \$deviceSecret
        deviceId: \$deviceId
        refreshToken: \$refreshToken
    ){
        accessToken
        refreshToken
    }
}
""");
