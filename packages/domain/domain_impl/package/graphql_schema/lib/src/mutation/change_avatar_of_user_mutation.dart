import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_schema/src/late_parse.dart';

class ChangeAvatarOfUserMutation extends Request {
  ChangeAvatarOfUserMutation(String file)
      : super(
          operation: _document.operation,
          variables: {
            "file": file,
          },
        );
}

final _document = LateParse(""" 
mutation changeAvatarOfUser(
    \$file:String
){
    changeAvatarOfUser(file:\$file){
        id
        publicUsername
        username
        avatar
    }
}
""");
