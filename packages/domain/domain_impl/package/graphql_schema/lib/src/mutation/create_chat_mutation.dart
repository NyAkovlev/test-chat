import 'package:gql_exec/gql_exec.dart';
import 'package:graphql_schema/src/late_parse.dart';

class CreateChatMutation extends Request {
  CreateChatMutation()
      : super(
          operation: _document.operation,
        );
}

final _document = LateParse(""" 
mutation createPersonalChat{
    createChat{
        id
        avatar
        canInvite
        name
        created
        updated
        deleted
    }
}
""");
