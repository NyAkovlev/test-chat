import 'dart:typed_data';

class LocalFileModel {
  final String id;
  final Future<Uint8List> Function()? _read;
  final Uint8List? _data;

  LocalFileModel(this.id, this._read) : _data = null;

  LocalFileModel.data(this.id, this._data) : _read = null;

  Future<Uint8List> read() async {
    if (_data != null) {
      return _data!;
    } else {
      return _read!();
    }
  }
}
