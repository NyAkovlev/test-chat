class MessageContentItemModel {
  final String name;
  final String value;

  MessageContentItemModel(this.name, this.value);

  static const text = "Text";
}
