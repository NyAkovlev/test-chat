import 'message_content_object_model.dart';

class MessageContentModel {
  final List<MessageContentItemModel> items;

  MessageContentModel(this.items);
}
