import 'message_content_model.dart';

class MessageModel {
  final int id;
  final int chatId;
  final DateTime created;
  final MessageContentModel content;
  final int sender;

  MessageModel(
    this.id,
    this.chatId,
    this.created,
    this.content,
    this.sender,
  );
}
