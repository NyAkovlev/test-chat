class UserModel {
  final int id;
  final String username;
  final String publicUsername;
  final String? avatar;

  UserModel(
    this.id,
    this.username,
    this.publicUsername,
    this.avatar,
  );
}
