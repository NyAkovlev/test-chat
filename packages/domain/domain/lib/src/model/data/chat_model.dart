class ChatModel {
  final int id;
  final String name;
  final String? avatar;
  final bool canInvite;
  final DateTime created;
  final DateTime updated;

  ChatModel(
    this.id,
    this.name,
    this.avatar,
    this.canInvite,
    this.created,
    this.updated,
  );
}
