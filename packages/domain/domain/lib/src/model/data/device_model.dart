class DeviceModel {
  final int id;
  final String name;
  final String? secret;

  DeviceModel(
    this.id,
    this.name,
    this.secret,
  );
}
