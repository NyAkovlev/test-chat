class ChatUserModel {
  final int userId;
  final int chatId;
  final DateTime invited;
  final String role;

  ChatUserModel(
    this.userId,
    this.chatId,
    this.invited,
    this.role,
  );
}
