import 'package:domain/src/model/data/chat_user/chat_user_permission.dart';

class ChatUserRole {
  final String name;
  final Set<ChatUserPermission> permission;

  ChatUserRole(this.name, this.permission);
}
