enum ChatUserPermission {
  DELETE,
  INVITE,
  KICK,
  LEAVE,
  WRITE,
  READ,
  RENAME,
  SET_AVATAR,
}

class ChatUserPermissionHelper {
  static final instance = ChatUserPermissionHelper._();
  final _fromStringValues = <ChatUserPermission, String>{};
  final _toStringValues = <String, ChatUserPermission>{};

  ChatUserPermissionHelper._() {
    ChatUserPermission.values.forEach((e) {
      final name = e.toString().split(".")[2];
      _fromStringValues[e] = name;
      _toStringValues[name] = e;
    });
  }

  ChatUserPermission modelFromString(String name) {
    return _toStringValues["name"]!;
  }

  String modelToString(ChatUserPermission model) {
    return _fromStringValues[model]!;
  }
}
