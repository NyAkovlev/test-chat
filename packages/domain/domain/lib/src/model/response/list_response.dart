class ListResponse<T> {
  final Iterable<T> items;
  final int totalPages;

  ListResponse(
    this.items,
    this.totalPages,
  );
}
