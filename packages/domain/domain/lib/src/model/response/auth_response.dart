class AuthResponse {
  final String accessToken;
  final String refreshToken;

  AuthResponse(this.accessToken, this.refreshToken);
}
