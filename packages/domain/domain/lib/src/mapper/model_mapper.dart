abstract class  ModelMapper<T> {
  Map<String, dynamic> toJson(T model);

  T toModel(Map<String, dynamic> map);
}
