import 'package:domain/export.dart';

abstract class UserDatabaseApi {
  Future<UserModel?> getById(int id);

  Future<void> save(UserModel userModel);
}
