import 'dart:typed_data';

import 'package:domain/export.dart';

abstract class CachedFileApi {
  Future<LocalFileModel?> get(String name);

  Future<void> savePath(String name, String path);

  Future<void> saveData(String name, Uint8List data);
}
