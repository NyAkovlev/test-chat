abstract class FilePickerApi {
  Future<String?> pickImage();
}
