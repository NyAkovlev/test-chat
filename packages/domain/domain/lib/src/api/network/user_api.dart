import 'package:domain/export.dart';

abstract class UserApi {
  Future<UserModel> getUser();

  Future<ListResponse<UserModel>> findUser(
    String username,
    int size,
    int page,
  );

  Future<UserModel> changeAvatar(String fileId);
}
