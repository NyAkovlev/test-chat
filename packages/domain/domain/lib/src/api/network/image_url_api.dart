abstract class ImageUrlApi {
  String getUrl(String id);
}
