abstract class TokenInvalidateListener {
  void addListener(Function() listener);

  void removeListener(Function() listener);
}
