import 'package:domain/export.dart';

abstract class DeviceApi {
  Future<DeviceModel> createDevice(String name, String secret);
}
