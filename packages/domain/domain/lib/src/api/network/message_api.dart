import 'package:domain/export.dart';

abstract class MessageApi {
  Future<ListResponse<MessageModel>> messagesOfChatBefore(
    int chatId,
    DateTime before,
    int size,
  );

  Future<MessageModel> sendMessage(
    int chatId,
    MessageContentModel messageContentModel,
  );
}
