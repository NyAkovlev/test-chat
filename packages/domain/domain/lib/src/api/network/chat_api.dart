import 'package:domain/export.dart';

abstract class ChatApi {
  Future<ListResponse<ChatModel>> chats(
    int size,
    int page,
  );

  Future<ListResponse<ChatUserModel>> memberOfChat(
    int size,
    int page,
    int chatId,
  );

  Future<ChatModel> createPersonalChat(int secondUserId);

  Future<ChatModel> createChat();

  Future<ChatModel?> personalChat(int secondUserId);

  Future deleteChat(int chatId);

  Future<List<ChatUserRole>> chatRole();

  Future<ChatModel> changeAvatar(int chatId, String fileId);
}
