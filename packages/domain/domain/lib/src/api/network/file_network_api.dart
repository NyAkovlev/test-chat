import 'dart:typed_data';

abstract class FileNetworkApi {
  Future uploadFile(String path, String uuid);

  Future<Uint8List> downloadFile(String uuid);
}
