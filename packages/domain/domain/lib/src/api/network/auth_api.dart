import 'package:domain/export.dart';

abstract class AuthApi {
  Future<AuthResponse> signIn(
    String deviceSecret,
    int deviceId,
    String username,
    String password,
  );

  Future<AuthResponse> signUp(
    String deviceSecret,
    int deviceId,
    String username,
    String password,
  );

  Future<AuthResponse> refreshToken(
    String deviceSecret,
    int deviceId,
    String refreshToken,
  );
}
