abstract class DeviceInfoApi {
  Future<String> deviceName();
}
