import 'package:domain/export.dart';

abstract class AuthPref {
  Pref<String> get accessToken;

  Pref<String> get refreshToken;

  Future<void> clear();
}
