import 'package:domain/export.dart';

abstract class DevicePref {
  Pref<String> get deviceSecret;

  Pref<String> get deviceName;

  Pref<int> get deviceId;

  Future clear();
}
