import 'package:domain/export.dart';

abstract class UserPref {
  Pref<UserModel> get user;

  Future<void> clear();
}
