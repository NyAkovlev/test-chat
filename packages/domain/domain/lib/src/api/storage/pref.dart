import 'package:domain/export.dart';
import 'package:meta/meta.dart';

abstract class Pref<T> {
  final String key;

  final _listener = <Function(T?, T?)>{};

  Pref(this.key);

  T? get();

  Future set(T? value);

  void addListeners(Function(T?, T?) function) {
    _listener.add(function);
  }

  void removeListener(Function(T?, T?) function) {
    _listener.remove(function);
  }

  @protected
 void notifyListeners(T? old, T? value) {
    for (var listener in _listener) {
      try {
        listener(old, value);
      } catch (e, s) {
        print("$s\n$e");
      }
    }
  }
}
