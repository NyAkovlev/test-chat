import 'dart:async';

import 'package:domain/export.dart';

abstract class SocketController {
  SocketState? get state;

  Stream<SocketState> get stateStream;

  Future<MessageSocketController> connectToChat(int id);

  void close();
}

enum SocketState {
  Connected,
  Error,
  Connecting,
  Done,
}
