import 'dart:async';

import 'package:domain/export.dart';

abstract class MessageSocketController {
  Stream<SocketMessageState> messageStream();

  void sendMessage(int chatId, MessageContentModel message);

  void close();
}
