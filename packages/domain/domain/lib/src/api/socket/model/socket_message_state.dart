import 'package:domain/export.dart';

class SocketMessageState {
  final String name;
  final MessageModel model;

  SocketMessageState(this.name, this.model);

  static const added = "Added";
  static const removed = "Removed";
  static const edited = "Edited";
}
