import 'package:app_di/app_di.dart';
import 'package:flutter/material.dart';

class ImageWidget extends StatefulWidget {
  final String? id;
  final double? height;
  final double? width;

  const ImageWidget({
    Key? key,
    required this.id,
    this.height,
    this.width,
  }) : super(key: key);

  @override
  _ImageWidgetState createState() => _ImageWidgetState();
}

class _ImageWidgetState extends State<ImageWidget> {
  final imageUrl = AppDi.instance.imageUrlApi();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: widget.height,
      width: widget.width,
      child: widget.id == null
          ? Icon(Icons.image)
          : Image.network(imageUrl.getUrl(widget.id!)),
    );
  }
}
