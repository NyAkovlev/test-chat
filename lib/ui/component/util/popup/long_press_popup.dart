import 'package:flutter/material.dart';

class LongPressPopupMenu<T> extends StatefulWidget {
  final Widget child;
  final PopupMenuItemBuilder<T> itemBuilder;
  final PopupMenuItemSelected<T> onSelected;

  const LongPressPopupMenu({
    required this.itemBuilder,
    required this.child,
    required this.onSelected,
  });

  @override
  _LongPressPopupMenuState<T> createState() => _LongPressPopupMenuState<T>();
}

class _LongPressPopupMenuState<T> extends State<LongPressPopupMenu<T>> {
  late Offset _tapDownPosition;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (TapDownDetails details) {
        _tapDownPosition = details.globalPosition;
      },
      onLongPress: () async {
        final overlay =
            Overlay.of(context)?.context.findRenderObject() as RenderBox?;
        if (overlay != null) {
          final value = await showMenu<T>(
            context: context,
            items: widget.itemBuilder(context),
            position: RelativeRect.fromLTRB(
              _tapDownPosition.dx,
              _tapDownPosition.dy,
              overlay.size.width - _tapDownPosition.dx,
              overlay.size.height - _tapDownPosition.dy,
            ),
          );
          if (value != null) {
            widget.onSelected(value);
          }
        }
      },
      child: widget.child,
    );
  }
}
