import 'package:app_bloc/export.dart';
import 'package:app_ui/app_ui.dart';
import 'package:domain/export.dart';
import 'package:flutter/cupertino.dart';

class User {
  static UserModel? of(BuildContext context) {
    return BlocProvider.of<UserBloc>(context).state.user;
  }
}
