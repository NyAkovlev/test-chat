import 'package:app_bloc/export.dart';
import 'package:app_ui/app_ui.dart';
import 'package:domain/export.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserListener extends StatelessWidget {
  final Function(UserModel?)? listener;
  final Function(BuildContext, UserModel?) builder;

  const UserListener({
    Key? key,
    this.listener,
    required this.builder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocWidget<UserBloc, UserState>(
      bloc: BlocProvider.of(context),
      listener: listener != null
          ? (_, state) {

                listener?.call(state.user!);
            }
          : null,
      builder: (context, state) {
        return builder(context, state.user!);
      },
    );
  }
}
