import 'dart:async';

import 'package:app_bloc/export.dart';
import 'package:app_ui/app_ui.dart';
import 'package:flutter/material.dart';

class PaginationWidget<Model> extends StatefulWidget {
  final PaginationBloc<Model> Function() bloc;
  final Widget Function(Model) itemBuilder;
  final VoidCallback? onRefresh;
  final int paginationFromIndex;

  const PaginationWidget({
    Key? key,
    required this.bloc,
    required this.itemBuilder,
    this.paginationFromIndex = 5,
    this.onRefresh,
  }) : super(key: key);

  @override
  _PaginationWithMapWidgetState<Model> createState() =>
      _PaginationWithMapWidgetState<Model>();
}

class _PaginationWithMapWidgetState<Model>
    extends State<PaginationWidget<Model>> {
  late PaginationBloc<Model> bloc;
  Completer? completer;
  bool isProgress = false;

  @override
  void initState() {
    super.initState();
    bloc = widget.bloc();
  }

  @override
  void dispose() {
    super.dispose();
    bloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocWidget<PaginationBloc<Model>, PaginationState<Model>>(
      bloc: bloc,
      listener: (_, state) {
        completer?.complete();
        completer = null;
      },
      builder: (BuildContext context, state) {
        return buildRefresh(child: buildList(state));
      },
    );
  }

  Widget buildRefresh({required Widget child}) {
    return RefreshIndicator(
      onRefresh: () {
        completer = Completer();
        bloc.add(PaginationEventRefresh());
        return completer!.future;
      },
      child: child,
    );
  }

  Widget buildList(PaginationState<Model> state) {
    final itemsLength = state.items.length;
    final hasError = state.error != null;
    var length = itemsLength;
    final hasAddedItem = state.hasMore || hasError;
    if (hasAddedItem) {
      length++;
    }
    return ListView.builder(
      itemCount: length,
      itemBuilder: (BuildContext context, int index) {
        if (!isProgress &&
            state.hasMore &&
            index + widget.paginationFromIndex >= itemsLength) {
          isProgress = true;
          bloc.add(PaginationEventNextPage());
        }
        if (hasAddedItem && index == length - 1) {
          return SizedBox(
            height: 60,
            child: Center(
              child: hasError ? Icon(Icons.error) : CircularProgressIndicator(),
            ),
          );
        } else {
          return widget.itemBuilder(
            state.items[index],
          );
        }
      },
    );
  }
}
