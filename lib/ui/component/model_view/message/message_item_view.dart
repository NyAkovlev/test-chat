import 'package:client/ui/component/bloc/user.dart';
import 'package:domain/export.dart';
import 'package:flutter/material.dart';

class MessageItemView extends StatefulWidget {
  final MessageModel messageModel;

  const MessageItemView({
    Key? key,
    required this.messageModel,
  }) : super(key: key);

  @override
  _MessageItemViewState createState() => _MessageItemViewState();
}

class _MessageItemViewState extends State<MessageItemView> {
  late String content;

  @override
  void initState() {
    super.initState();
    buildContent();
  }

  void buildContent() {
    var content = <String>[];
    for (var value in widget.messageModel.content.items) {
      if (value.name == MessageContentItemModel.text) {
        content.add(value.value);
      }
    }
    this.content = content.join("\n");
  }

  @override
  Widget build(BuildContext context) {
    final user = User.of(context)!;
    final message = widget.messageModel;
    final isMy = message.sender == user.id;
    return ListTile(
      contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 0),
      leading: Icon(Icons.camera_front),
      title: Text(content),
    );
  }
}
