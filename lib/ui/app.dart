import 'package:app_bloc/export.dart';
import 'package:app_di/app_di.dart';
import 'package:app_ui/app_ui.dart';
import 'package:client/ui/navigator/app_navigator.dart';
import 'package:client/ui/navigator/navigation_route.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final navigatorKey = GlobalKey<NavigatorState>();
  UserBloc? userBloc;
  late ErrorBloc errorBloc;
  late InvalidationTokenBloc invalidationTokenBloc;
  late AppNavigator _appNavigator;

  @override
  void initState() {
    super.initState();
    init();
  }

  @override
  void dispose() {
    super.dispose();
    userBloc?.close();
    errorBloc.close();
    invalidationTokenBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    if (userBloc != null) {
      return Provider.value(
        value: _appNavigator,
        child: BlocListener<InvalidationTokenBloc, InvalidationTokenState>(
          bloc: invalidationTokenBloc,
          listener: (_, state) {
            if (state is InvalidationTokenStateInvalidated) {
              _appNavigator.add(NavigationRoutes.authRoute());
            }
          },
          child: BlocWidget<UserBloc, UserState>(
              bloc: userBloc!,
              builder: (context, snapshot) {
                return BlocProvider(
                  bloc: errorBloc,
                  child: MaterialApp(
                    navigatorKey: navigatorKey,
                    navigatorObservers: [_appNavigator as NavigatorObserver],
                    onGenerateRoute: _appNavigator.initialRoute,
                  ),
                );
              }),
        ),
      );
    } else {
      return SizedBox.shrink();
    }
  }

  Future init() async {
    await AppDi.init();
    userBloc = AppDi.instance.userBloc();
    errorBloc = AppDi.instance.errorBloc();
    invalidationTokenBloc = AppDi.instance.invalidationTokenBloc();
    _appNavigator = AppNavigatorImpl(AppDi.instance.authPref());
    setState(() {});
  }
}
