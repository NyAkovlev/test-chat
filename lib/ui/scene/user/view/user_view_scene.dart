import 'package:app_bloc/export.dart';
import 'package:client/ui/component/util/image/cached_image_widget.dart';
import 'package:client/ui/navigator/app_navigator.dart';
import 'package:client/ui/navigator/export.dart';
import 'package:domain/export.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class UserViewScene extends StatefulWidget {
  final UserModel user;

  const UserViewScene({
    Key? key,
    required this.user,
  }) : super(key: key);

  @override
  _UserViewSceneState createState() => _UserViewSceneState();
}

class _UserViewSceneState extends State<UserViewScene> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          ImageWidget(
            height: 100,
            width: 100,
            id: widget.user.avatar,
          ),
          ListTile(
            title: Text(widget.user.publicUsername),
            trailing: TextButton(
              child: Text("Write"),
              onPressed: () {
                Provider.of<AppNavigator>(context, listen: false).add(
                  NavigationRoutes.chatCreationRoute(
                    ChatCreationEventPersonalCreate(widget.user),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
