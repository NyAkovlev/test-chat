import 'package:app_di/app_di.dart';
import 'package:client/ui/component/bloc/pagination_widget.dart';
import 'package:client/ui/scene/user/view/user_view_scene.dart';
import 'package:domain/export.dart';
import 'package:flutter/material.dart';

class UserSearch extends SearchDelegate<UserModel?> {

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      if (query.isNotEmpty)
        IconButton(
          tooltip: 'Clear',
          icon: const Icon(Icons.clear),
          onPressed: () {
            query = '';
            showSuggestions(context);
          },
        ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      tooltip: 'Back',
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    throw UnimplementedError();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return PaginationWidget<UserModel>(
      key: ValueKey(query),
      itemBuilder: (e) {
        return ListTile(
          title: Text(e.publicUsername),
          onTap: () {
            close(context,e);
          },
        );
      },
      bloc: () {
        return AppDi.instance.userPaginationBloc(query);
      },
    );
  }
}
