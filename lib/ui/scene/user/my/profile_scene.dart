import 'package:client/ui/component/bloc/user.dart';
import 'package:flutter/material.dart';

import 'change_user_avatar_widget.dart';

class ProfileScene extends StatefulWidget {
  const ProfileScene({
    Key? key,
  }) : super(key: key);

  @override
  _ProfileSceneState createState() => _ProfileSceneState();
}

class _ProfileSceneState extends State<ProfileScene> {
  @override
  Widget build(BuildContext context) {
    final user = User.of(context);
    if (user == null) {
      return SizedBox.shrink();
    }
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          ChangeUserAvatarWidget(
            user: user,
          ),
          ListTile(
            title: Text(user.publicUsername),
          )
        ],
      ),
    );
  }
}
