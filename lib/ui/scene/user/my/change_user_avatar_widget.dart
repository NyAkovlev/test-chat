import 'package:app_bloc/export.dart';
import 'package:app_di/app_di.dart';
import 'package:app_ui/app_ui.dart';
import 'package:client/ui/component/util/image/cached_image_widget.dart';
import 'package:domain/export.dart';
import 'package:flutter/material.dart';

class ChangeUserAvatarWidget extends StatefulWidget {
  final UserModel user;

  const ChangeUserAvatarWidget({Key? key, required this.user})
      : super(key: key);

  @override
  _ChangeUserAvatarWidgetState createState() => _ChangeUserAvatarWidgetState();
}

class _ChangeUserAvatarWidgetState extends State<ChangeUserAvatarWidget> {
  final bloc = AppDi.instance.userChangeAvatarBloc();

  @override
  void dispose() {
    super.dispose();
    bloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocWidget<UserChangeAvatarBloc, UserChangeAvatarState>(
      bloc: bloc,
      builder: (context, state) {
        final progress = state is UserChangeAvatarStateProgress;
        return InkWell(
          onTap: progress
              ? null
              : () {
                  bloc.add(UserChangeAvatarEventSet());
                },
          child: ImageWidget(
            height: 100,
            width: 100,
            id: widget.user.avatar,
          ),
        );
      },
    );
  }
}
