import 'package:app_bloc/export.dart';
import 'package:app_di/app_di.dart';
import 'package:app_ui/app_ui.dart';
import 'package:client/ui/navigator/app_navigator.dart';
import 'package:client/ui/navigator/export.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ChatCreationScene extends StatefulWidget {
  final ChatCreationEvent event;

  const ChatCreationScene({
    Key? key,
    required this.event,
  }) : super(key: key);

  @override
  _ChatCreationSceneState createState() => _ChatCreationSceneState();
}

class _ChatCreationSceneState extends State<ChatCreationScene> {
  final bloc = AppDi.instance.chatCreationBloc();

  @override
  void initState() {
    super.initState();
    bloc.add(widget.event);
  }

  @override
  void dispose() {
    super.dispose();
    bloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocWidget<ChatCreationBloc, ChatCreationState>(
        bloc: bloc,
        listener: (_, state) {
          if (state is ChatCreationStateSuccess) {
            Provider.of<AppNavigator>(context, listen: false).add(
              NavigationRoutes.chatViewRoute(
                state.chatModel,
                NavigationMethods.replace(),
              ),
            );
          }
        },
        builder: (context, state) {
          if (state is ChatCreationStateError) {
            return Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(Icons.error),
                  Text(state.error.toString()),
                  TextButton(
                    child: Text("retry"),
                    onPressed: () {
                      bloc.add(widget.event);
                    },
                  ),
                ],
              ),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
