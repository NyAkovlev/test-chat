import 'package:app_di/app_di.dart';
import 'package:client/ui/scene/chat/view/chat_view_scene.dart';
import 'package:domain/export.dart';
import 'package:flutter/material.dart';

class ChatConnectionScene extends StatefulWidget {
  final ChatModel chatModel;

  const ChatConnectionScene({Key? key, required this.chatModel})
      : super(key: key);

  @override
  _ChatConnectionSceneState createState() => _ChatConnectionSceneState();
}

class _ChatConnectionSceneState extends State<ChatConnectionScene> {
  final socketController = AppDi.instance.socketController();
  late Future<MessageSocketController> future;

  @override
  void initState() {
    super.initState();
    future = socketController.connectToChat(widget.chatModel.id);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<SocketState>(
      initialData: socketController.state,
      stream: socketController.stateStream,
      builder: (context, result) {
        final state = result.data;
        switch (state) {
          case SocketState.Connected:
            return FutureBuilder<MessageSocketController>(
              future: future,
              builder: (BuildContext context,
                  AsyncSnapshot<MessageSocketController> snapshot) {
                if (snapshot.data != null) {
                  return ChatViewScene(
                    chatModel: widget.chatModel,
                    messageSocketController: snapshot.data!,
                  );
                } else {
                  return Center(
                    child: Text("Connecting"),
                  );
                }
              },
            );
          case null:
          case SocketState.Connecting:
            return Center(
              child: Text("Connecting"),
            );
          case SocketState.Done:
            return Center(
              child: Text("Done"),
            );
          case SocketState.Error:
            return Center(
              child: Text("Error"),
            );
        }
      },
    );
  }
}
