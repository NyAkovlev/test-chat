import 'package:app_di/app_di.dart';
import 'package:client/ui/component/bloc/pagination_widget.dart';
import 'package:client/ui/navigator/app_navigator.dart';
import 'package:client/ui/navigator/export.dart';
import 'package:client/ui/scene/user/search/user_search_delegate.dart';
import 'package:domain/export.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ChatListScene extends StatefulWidget {
  @override
  _ChatListSceneState createState() => _ChatListSceneState();
}

class _ChatListSceneState extends State<ChatListScene> {
  final searchDelegate = UserSearch();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.supervised_user_circle_sharp),
          onPressed: () {
            Provider.of<AppNavigator>(context, listen: false)
                .add(NavigationRoutes.profileRoute(NavigationMethods.push()));
          },
        ),
        actions: [
          IconButton(
            tooltip: 'Search',
            icon: const Icon(Icons.search),
            onPressed: () async {
              final selectedUser = await showSearch(
                context: context,
                delegate: searchDelegate,
              );
              if (selectedUser != null) {
                Provider.of<AppNavigator>(context, listen: false)
                    .add(NavigationRoutes.userViewRoute(selectedUser));
              }
            },
          ),
        ],
      ),
      body: PaginationWidget<ChatModel>(
        itemBuilder: (e) {
          return ListTile(
            title: Text(e.name),
            onTap: () {
              Provider.of<AppNavigator>(context, listen: false).add(
                NavigationRoutes.chatViewRoute(
                  e,
                  NavigationMethods.push(),
                ),
              );
            },
          );
        },
        bloc: () {
          return AppDi.instance.chatPaginationBloc();
        },
      ),
    );
  }
}
