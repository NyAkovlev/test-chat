import 'package:app_bloc/export.dart';
import 'package:app_di/app_di.dart';
import 'package:app_ui/app_ui.dart';
import 'package:client/ui/component/bloc/user_listener.dart';
import 'package:client/ui/scene/message/message_list_widget.dart';
import 'package:domain/export.dart';
import 'package:flutter/material.dart';

class ChatViewScene extends StatefulWidget {
  final ChatModel chatModel;
  final MessageSocketController messageSocketController;

  const ChatViewScene({
    Key? key,
    required this.chatModel,
    required this.messageSocketController,
  }) : super(key: key);

  @override
  _ChatViewSceneState createState() => _ChatViewSceneState();
}

class _ChatViewSceneState extends State<ChatViewScene> {
  late MessageListenerBloc listenerBloc;
  final scrollController = ScrollController();
  var paginationProgress = false;

  @override
  void initState() {
    super.initState();
    listenerBloc = AppDi.instance.messageListenerBloc(
      widget.messageSocketController,
    );
  }

  @override
  void dispose() {
    listenerBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocWidget<MessageListenerBloc, MessageListenerState>(
      bloc: listenerBloc,
      listener: (_, state) {
        if (scrollController.position.minScrollExtent ==
            scrollController.offset) {
          WidgetsBinding.instance?.addPostFrameCallback((_) {
            scrollController.animateTo(
              scrollController.position.minScrollExtent,
              duration: Duration(milliseconds: 300),
              curve: Curves.ease,
            );
          });
        }
      },
      builder: (context, listenerState) {
        return Scaffold(
          appBar: AppBar(
            title: Text(widget.chatModel.name),
          ),
          body: MessageListWidget(
            listenerState: listenerState,
            chatModel: widget.chatModel,
            scrollController: scrollController,
          ),
        );
      },
    );
  }
}
