import 'package:app_bloc/export.dart';
import 'package:app_di/app_di.dart';
import 'package:app_ui/app_ui.dart';
import 'package:domain/export.dart';
import 'package:flutter/material.dart';

class SendMessageView extends StatefulWidget {
  final ChatModel chatModel;

  const SendMessageView({
    Key? key,
    required this.chatModel,
  }) : super(key: key);

  @override
  _SendMessageViewState createState() => _SendMessageViewState();
}

class _SendMessageViewState extends State<SendMessageView> {
  late MessageSendingBloc sendingBloc;
  final inputController = TextEditingController();

  @override
  void initState() {
    super.initState();

    sendingBloc = AppDi.instance.messageSendingBloc(
      widget.chatModel,
    );
  }

  @override
  void dispose() {
    sendingBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocWidget<MessageSendingBloc, MessageSendingState>(
      bloc: sendingBloc,
      listener: (_, state) {
        if (state is MessageSendingStateSuccess) {
          inputController.text = "";
        }
      },
      builder: (context, state) {
        final isProgress = state is MessageSendingStateProgress;
        return Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: TextFormField(
                  decoration:
                      InputDecoration.collapsed(hintText: "Write a message"),
                  controller: inputController,
                ),
              ),
            ),
            SizedBox(
              width: 48,
              height: 48,
              child: isProgress
                  ? Center(
                      child: SizedBox(
                          width: 24,
                          height: 24,
                          child: CircularProgressIndicator()),
                    )
                  : IconButton(
                      icon: Icon(Icons.send),
                      onPressed: () {
                        sendingBloc.add(
                          MessageSendingEventSend(
                            MessageContentModel(
                              [
                                MessageContentItemModel(
                                  MessageContentItemModel.text,
                                  inputController.text,
                                )
                              ],
                            ),
                          ),
                        );
                      },
                    ),
            ),
          ],
        );
      },
    );
  }
}
