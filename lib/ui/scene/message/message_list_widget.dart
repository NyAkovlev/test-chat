import 'package:app_bloc/export.dart';
import 'package:app_di/app_di.dart';
import 'package:app_ui/app_ui.dart';
import 'package:client/ui/component/model_view/message/message_item_view.dart';
import 'package:client/ui/scene/message/send_message_widget.dart';
import 'package:domain/export.dart';
import 'package:flutter/material.dart';

class MessageListWidget extends StatefulWidget {
  final MessageListenerState listenerState;
  final ChatModel chatModel;
  final ScrollController scrollController;

  const MessageListWidget({
    Key? key,
    required this.listenerState,
    required this.chatModel,
    required this.scrollController,
  }) : super(key: key);

  @override
  _MessageListWidgetState createState() => _MessageListWidgetState();
}

class _MessageListWidgetState extends State<MessageListWidget> {
  final inputController = TextEditingController();
  final sliverCenterKey = UniqueKey();
  var paginationProgress = false;
  late MessagePaginationBloc paginationBloc;

  @override
  void initState() {
    super.initState();
    paginationBloc = AppDi.instance.messagePaginationBloc(
      widget.chatModel,
    );
  }

  @override
  void dispose() {
    paginationBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocWidget<MessagePaginationBloc, PaginationState<MessageModel>>(
        bloc: paginationBloc,
        listener: (_, state) {
          paginationProgress = false;
        },
        builder: (context, paginationState) {
          return Column(
            children: [
              Expanded(
                child: CustomScrollView(
                  controller: widget.scrollController,
                  center: sliverCenterKey,
                  reverse: true,
                  slivers: [
                    SliverList(
                      delegate: buildListenerList(widget.listenerState),
                    ),
                    SliverList(
                      key: sliverCenterKey,
                      delegate: buildPaginationList(paginationState),
                    ),
                  ],
                ),
              ),
              SendMessageView(chatModel: widget.chatModel),
            ],
          );
        });
  }

  SliverChildBuilderDelegate buildPaginationList(
      PaginationState<MessageModel> state) {
    final itemsLength = state.items.length;
    final hasError = state.error != null;
    var length = itemsLength;
    final hasAddedItem = state.hasMore || hasError;
    if (hasAddedItem) {
      length++;
    }
    return SliverChildBuilderDelegate(
      (_, i) {
        final index = i;
        if (!paginationProgress && state.hasMore && index + 5 >= itemsLength) {
          paginationProgress = true;
          paginationBloc.add(PaginationEventNextPage());
        }
        if (hasAddedItem && index == length - 1) {
          return SizedBox(
            height: 60,
            child: Center(
              child: hasError ? Icon(Icons.error) : CircularProgressIndicator(),
            ),
          );
        } else {
          final item = state.items[index];
          return buildMessage(item);
        }
      },
      childCount: length,
    );
  }

  SliverChildBuilderDelegate buildListenerList(MessageListenerState state) {
    return SliverChildBuilderDelegate(
      (_, i) {
        final index = i;
        final item = state.message[index];
        return buildMessage(item);
      },
      childCount: state.message.length,
    );
  }

  MessageItemView buildMessage(MessageModel model) {
    return MessageItemView(
      key: ValueKey(model.id),
      messageModel: model,
    );
  }
}
