import 'package:app_bloc/export.dart';
import 'package:app_di/app_di.dart';
import 'package:app_ui/app_ui.dart';
import 'package:client/ui/navigator/app_navigator.dart';
import 'package:client/ui/navigator/export.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AuthScene extends StatefulWidget {
  @override
  _AuthSceneState createState() => _AuthSceneState();
}

class _AuthSceneState extends State<AuthScene> {
  final bloc = AppDi.instance.authBloc();
  final loginCtrl = TextEditingController();
  final passCtrl = TextEditingController();

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: BlocWidget<AuthBloc, AuthState>(
            bloc: bloc,
            listener: (_, state) {
              if (state is AuthStateSuccess) {
                Provider.of<AppNavigator>(context,listen: false)
                    .add(NavigationRoutes.chatListRoute());
              }
            },
            builder: (context, state) {
              final isProgress = state is AuthStateProgress;
              return Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        controller: loginCtrl,
                        decoration:
                            InputDecoration.collapsed(hintText: "login"),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        controller: passCtrl,
                        decoration: InputDecoration.collapsed(hintText: "pass"),
                      ),
                    ),
                    if (isProgress)
                      CircularProgressIndicator()
                    else ...[
                      OutlinedButton(
                        onPressed: () {
                          bloc.add(
                            AuthEvents.signInByLogin(
                              loginCtrl.text,
                              passCtrl.text,
                            ),
                          );
                        },
                        child: Text("Sign in"),
                      ),
                      OutlinedButton(
                        onPressed: () {
                          bloc.add(
                            AuthEvents.signUpByLogin(
                              loginCtrl.text,
                              passCtrl.text,
                            ),
                          );
                        },
                        child: Text("Sign up"),
                      ),
                    ]
                  ],
                ),
              );
            }),
      ),
    );
  }
}
