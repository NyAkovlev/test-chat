// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'navigation_method.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class NavigationMethods {
  static Push push() {
    return Push();
  }

  static Replace replace([
    List<NavigationRoute>? until,
  ]) {
    return Replace(until);
  }

  static Root root() {
    return Root();
  }

  static Dialog dialog() {
    return Dialog();
  }

  static BottomSheet bottomSheet() {
    return BottomSheet();
  }

  static Overlay overlay(
    Duration duration,
  ) {
    return Overlay(duration);
  }
}

abstract class NavigationMethodSwitch<O>
    implements SealedSwitch<NavigationMethod, O> {
  O switchCase(NavigationMethod type) => type is Push
      ? onPush(type)
      : type is Replace
          ? onReplace(type)
          : type is Root
              ? onRoot(type)
              : type is Dialog
                  ? onDialog(type)
                  : type is BottomSheet
                      ? onBottomSheet(type)
                      : type is Overlay
                          ? onOverlay(type)
                          : onDefault(type);
  O onPush(Push push);
  O onReplace(Replace replace);
  O onRoot(Root root);
  O onDialog(Dialog dialog);
  O onBottomSheet(BottomSheet bottomSheet);
  O onOverlay(Overlay overlay);
  O onDefault(NavigationMethod navigationMethod) => throw UnimplementedError();
}

class NavigationMethodSwitchImpl<O> implements NavigationMethodSwitch<O> {
  final O Function(Push)? _push;
  final O Function(Replace)? _replace;
  final O Function(Root)? _root;
  final O Function(Dialog)? _dialog;
  final O Function(BottomSheet)? _bottomSheet;
  final O Function(Overlay)? _overlay;
  final O Function(NavigationMethod)? _navigationMethod;

  NavigationMethodSwitchImpl({
    O Function(Push)? push,
    O Function(Replace)? replace,
    O Function(Root)? root,
    O Function(Dialog)? dialog,
    O Function(BottomSheet)? bottomSheet,
    O Function(Overlay)? overlay,
    O Function(NavigationMethod)? navigationMethod,
  })  : _push = push,
        _replace = replace,
        _root = root,
        _dialog = dialog,
        _bottomSheet = bottomSheet,
        _overlay = overlay,
        _navigationMethod = navigationMethod;

  @Deprecated("use base constructor or required")
  NavigationMethodSwitchImpl.fill(
    this._push,
    this._replace,
    this._root,
    this._dialog,
    this._bottomSheet,
    this._overlay,
    this._navigationMethod,
  );

  NavigationMethodSwitchImpl.required({
    required O Function(Push)? push,
    required O Function(Replace)? replace,
    required O Function(Root)? root,
    required O Function(Dialog)? dialog,
    required O Function(BottomSheet)? bottomSheet,
    required O Function(Overlay)? overlay,
    required O Function(NavigationMethod)? navigationMethod,
  })  : _push = push,
        _replace = replace,
        _root = root,
        _dialog = dialog,
        _bottomSheet = bottomSheet,
        _overlay = overlay,
        _navigationMethod = navigationMethod;

  O switchCase(NavigationMethod type) => (type is Push && _push != null)
      ? onPush(type)
      : (type is Replace && _replace != null)
          ? onReplace(type)
          : (type is Root && _root != null)
              ? onRoot(type)
              : (type is Dialog && _dialog != null)
                  ? onDialog(type)
                  : (type is BottomSheet && _bottomSheet != null)
                      ? onBottomSheet(type)
                      : (type is Overlay && _overlay != null)
                          ? onOverlay(type)
                          : (type is NavigationMethod &&
                                  _navigationMethod != null)
                              ? onDefault(type)
                              : throw UnimplementedError();

  O onPush(Push value) => _push!(value);
  O onReplace(Replace value) => _replace!(value);
  O onRoot(Root value) => _root!(value);
  O onDialog(Dialog value) => _dialog!(value);
  O onBottomSheet(BottomSheet value) => _bottomSheet!(value);
  O onOverlay(Overlay value) => _overlay!(value);
  O onDefault(NavigationMethod value) => _navigationMethod!(value);
}
