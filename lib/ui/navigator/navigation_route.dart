import 'package:app_bloc/export.dart';
import 'package:client/ui/navigator/navigation_method.dart';
import 'package:client/ui/scene/auth/auth_scene.dart';
import 'package:client/ui/scene/chat/connection/chat_connection_scene.dart';
import 'package:client/ui/scene/chat/creation/chat_creation_scene.dart';
import 'package:client/ui/scene/chat/list/chat_list_scene.dart';
import 'package:client/ui/scene/user/my/profile_scene.dart';
import 'package:client/ui/scene/user/view/user_view_scene.dart';
import 'package:domain/export.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:like_sealed/like_sealed.dart';

part 'navigation_route.sealed.dart';

/// Намерение в навигатор, которое новый роут с виджетом из [build]
///
/// не добавляет роут, если текущий равен ему. Это проверяется с помощью [Equatable]
/// и сравнению [props]
///
/// можно отключить эту проверку с помошью параметра [forcePush], присвоив ему true
@likeSealed
abstract class NavigationRoute extends Equatable {
  final NavigationMethod method;
  final bool forcePush;
  final stringify = true;

  List<Object> get props => [];

  NavigationRoute(this.method, [this.forcePush = false]);

  Widget build(BuildContext context);
}

class ChatListRoute extends NavigationRoute {
  ChatListRoute() : super(NavigationMethods.root());

  @override
  Widget build(BuildContext context) {
    return ChatListScene();
  }
}

class AuthRoute extends NavigationRoute {
  AuthRoute() : super(NavigationMethods.root());

  @override
  Widget build(BuildContext context) {
    return AuthScene();
  }
}

class UserViewRoute extends NavigationRoute {
  final UserModel user;

  UserViewRoute(this.user) : super(NavigationMethods.push());

  @override
  Widget build(BuildContext context) {
    return UserViewScene(user: user);
  }
}

class ChatCreationRoute extends NavigationRoute {
  final ChatCreationEvent event;

  ChatCreationRoute(this.event) : super(NavigationMethods.push());

  @override
  Widget build(BuildContext context) {
    return ChatCreationScene(event: event);
  }
}

class ChatViewRoute extends NavigationRoute {
  final ChatModel chatModel;

  ChatViewRoute({
    required this.chatModel,
    required NavigationMethod method,
  }) : super(method);

  @override
  Widget build(BuildContext context) {
    return ChatConnectionScene(chatModel: chatModel);
  }
}

class ProfileRoute extends NavigationRoute {
  ProfileRoute({
    required NavigationMethod method,
  }) : super(method);

  @override
  Widget build(BuildContext context) {
    return ProfileScene();
  }
}
