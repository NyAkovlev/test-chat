import 'package:flutter/cupertino.dart';
import 'package:client/ui/navigator/export.dart';

class AppRouteSettings extends RouteSettings {
  final NavigationRoute route;
  final NavigationMethod? method;

  const AppRouteSettings(this.route, this.method) : super(name: "$route");
}
