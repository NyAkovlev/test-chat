import 'package:client/ui/navigator/navigation_route.dart';
import 'package:like_sealed/like_sealed.dart';

part 'navigation_method.sealed.dart';

/// способ перехода на новый роут
@LikeSealed(switchImpl: true)
abstract class NavigationMethod {
  const NavigationMethod();
}

/// обычное добавление в список роутов
class Push extends NavigationMethod {}

/// добавление с заменой
/// если указан [until] то удаляет роуты, до первого в списке [until]
class Replace extends NavigationMethod {
  final List<NavigationRoute>? until;

  const Replace([this.until]);
}

/// очищает список роутов
class Root extends NavigationMethod {
  const Root();
}

/// открывает роут как Dialog
class Dialog extends NavigationMethod {
  const Dialog();
}

/// открывает роут как BottomSheet
class BottomSheet extends NavigationMethod {
  const BottomSheet();
}

/// открывает роут как Overlay
/// [duration] - через сколько роут будет удален
class Overlay extends NavigationMethod {
  final Duration duration;

  const Overlay(this.duration);
}
