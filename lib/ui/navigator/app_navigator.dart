import 'package:client/ui/navigator/navigation_method.dart';
import 'package:client/ui/navigator/navigation_route.dart';
import 'package:client/ui/navigator/route/app_route_settings.dart';
import 'package:client/ui/navigator/route/bottom_sheet_route.dart';
import 'package:domain/export.dart';
import 'package:flutter/material.dart';

/// навигатор использующийся для перехода между экранами, открытия BottomSheet
/// отоброжение Toast и диалогов
abstract class AppNavigator {
  /// добавление [NavigationRoute]
  Future add(NavigationRoute route);

  /// текущий [NavigationRoute]
  NavigationRoute? get currentNavigationRoute;

  /// возращение на шаг назад
  /// аналог Navigator.pop(context,result)
  void back([result]);

  /// возрашение до роута [until]
  void backUntil(NavigationRoute until);

  /// используется в [MaterialApp] для открытие первого экрана
  Route initialRoute(RouteSettings settings);

  /// листенер для [currentNavigationRoute]
  void addRouteListener(Function(NavigationRoute?) listener);

  void removeRouteListener(Function(NavigationRoute?) listener);
}

class AppNavigatorImpl extends NavigatorObserver implements AppNavigator {
  final Set<Function(NavigationRoute?)> _listeners = {};
  final AuthPref _authPref;
  late Route _current;

  NavigationRoute? get currentNavigationRoute =>
      currentRoute.settings is AppRouteSettings
          ? (currentRoute.settings as AppRouteSettings).route
          : null;

  Route get currentRoute => _current;

  set currentRoute(Route value) {
    _listeners.forEach(
      (element) => element((value.settings is AppRouteSettings
          ? (value.settings as AppRouteSettings).route
          : null)!),
    );
    _current = value;
  }

  notifyListeners(NavigationRoute? route) {
    for (var item in _listeners) {
      try {
        item.call(route);
      } catch (e, s) {
        print(s);
      }
    }
  }

  AppNavigatorImpl(this._authPref);

  Route initialRoute(RouteSettings settings) {
    Route _initRoute(NavigationRoute route) {
      return _routeWrap(
        route.build(navigator!.context),
        _routeSetting(
          route,
          NavigationMethods.root(),
        ),
      );
    }

    if (_authPref.accessToken.get() == null) {
      return _initRoute(NavigationRoutes.authRoute());
    } else {
      return _initRoute(NavigationRoutes.chatListRoute());
    }
  }

  void addRouteListener(Function(NavigationRoute?) listener) {
    _listeners.add(listener);
  }

  void removeRouteListener(Function(NavigationRoute?) listener) {
    _listeners.remove(listener);
  }

  AppRouteSettings _routeSetting(
    NavigationRoute route,
    NavigationMethod? method,
  ) {
    return AppRouteSettings(
      route,
      method,
    );
  }

  Future add(NavigationRoute route) async {
    final widget = route.build(navigator!.overlay!.context);
    final routeSetting = _routeSetting(route, route.method);
    if (!route.forcePush &&
        currentRoute.settings is AppRouteSettings &&
        (currentRoute.settings as AppRouteSettings).method == route.method) {
      return null;
    }

    return NavigationMethodSwitchImpl<Future>.required(
      push: (route) {
        return navigator!.push(_routeWrap(widget, routeSetting));
      },
      replace: (route) {
        final until = route.until;
        if (until != null) {
          navigator!.pushAndRemoveUntil(
            _routeWrap(widget, routeSetting),
            (route) {
              if (route.settings is AppRouteSettings) {
                final settings = route.settings as AppRouteSettings;
                for (var value in until) {
                  if (settings.route == value) {
                    return true;
                  }
                }
              }
              return false;
            },
          );
        }
        return navigator!.pushReplacement(_routeWrap(widget, routeSetting));
      },
      root: (route) {
        return navigator!.pushAndRemoveUntil(
          _routeWrap(widget, routeSetting),
          (_) => false,
        );
      },
      dialog: (route) {
        return _dialogWrap(widget, routeSetting);
      },
      bottomSheet: (route) {
        return navigator!.push(_bottomSheetRouteWrap(widget, routeSetting));
      },
      overlay: (route) async {
        final entry = OverlayEntry(builder: (_) => widget);
        final overlay = navigator!.overlay!;
        overlay.insert(entry);
        await Future.delayed(route.duration);
        entry.remove();
      },
      navigationMethod: (_) => throw UnimplementedError(),
    ).switchCase(route.method);
  }

  void didPush(Route<dynamic> route, Route<dynamic>? previousRoute) {
    currentRoute = route;
  }

  void didPop(Route<dynamic> route, Route<dynamic>? previousRoute) {
    currentRoute = previousRoute!;
  }

  void didReplace({Route<dynamic>? newRoute, Route<dynamic>? oldRoute}) {
    currentRoute = newRoute!;
  }

  Route _routeWrap(
    Widget widget,
    RouteSettings routeSettings,
  ) {
    return MaterialPageRoute(
      builder: (_) => widget,
      settings: routeSettings,
    );
  }

  Route _bottomSheetRouteWrap(
    Widget widget,
    RouteSettings routeSettings,
  ) {
    return BottomSheetRoute(
      builder: (_) => widget,
      settings: routeSettings,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      isDismissible: true,
      enableDrag: true,
    );
  }

  Future _dialogWrap(Widget widget, RouteSettings routeSettings) {
    return showDialog(
      routeSettings: routeSettings,
      context: navigator!.overlay!.context,
      builder: (context) {
        return widget;
      },
    );
  }

  void back([result]) {
    return navigator!.pop(result);
  }

  void backUntil(NavigationRoute until) {
    final untilSetting = _routeSetting(until, null);
    return navigator!
        .popUntil((route) => route.settings.name == untilSetting.name);
  }
}
