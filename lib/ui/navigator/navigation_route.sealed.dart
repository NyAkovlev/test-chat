// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'navigation_route.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class NavigationRoutes {
  static ChatListRoute chatListRoute() {
    return ChatListRoute();
  }

  static AuthRoute authRoute() {
    return AuthRoute();
  }

  static UserViewRoute userViewRoute(
    UserModel user,
  ) {
    return UserViewRoute(user);
  }

  static ChatCreationRoute chatCreationRoute(
    ChatCreationEvent event,
  ) {
    return ChatCreationRoute(event);
  }

  static ChatViewRoute chatViewRoute(
    ChatModel chatModel,
    NavigationMethod method,
  ) {
    return ChatViewRoute(chatModel: chatModel, method: method);
  }

  static ProfileRoute profileRoute(
    NavigationMethod method,
  ) {
    return ProfileRoute(method: method);
  }
}

abstract class NavigationRouteSwitch<O>
    implements SealedSwitch<NavigationRoute, O> {
  O switchCase(NavigationRoute type) => type is ChatListRoute
      ? onChatListRoute(type)
      : type is AuthRoute
          ? onAuthRoute(type)
          : type is UserViewRoute
              ? onUserViewRoute(type)
              : type is ChatCreationRoute
                  ? onChatCreationRoute(type)
                  : type is ChatViewRoute
                      ? onChatViewRoute(type)
                      : type is ProfileRoute
                          ? onProfileRoute(type)
                          : onDefault(type);
  O onChatListRoute(ChatListRoute chatListRoute);
  O onAuthRoute(AuthRoute authRoute);
  O onUserViewRoute(UserViewRoute userViewRoute);
  O onChatCreationRoute(ChatCreationRoute chatCreationRoute);
  O onChatViewRoute(ChatViewRoute chatViewRoute);
  O onProfileRoute(ProfileRoute profileRoute);
  O onDefault(NavigationRoute navigationRoute) => throw UnimplementedError();
}
